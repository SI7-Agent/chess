#ifndef CLIENT_H
#define CLIENT_H

#include <string>

class Client
{
protected:
    std::string m_ip;
public:
    Client():
        m_ip(std::string("localhost"))
    {};
    Client(std::string ip):
        m_ip(ip)
    {};
    void connect();
    void disconnect();
    void send(std::string state);
};

#endif

