#ifndef FIGURE_H
#define FIGURE_H

#include <bits/stdc++.h>
#include <string>
#include <utility>
#include <vector>

#include "../chessboard/chessboard.h"
#include "move.h"

#define EMPTY	0x00	// Pusto/Empty
#define PAWN	0x01	// Peshka/Pawn
#define ROOK	0x02	// Lad'ya/Rook
#define KNIGHT  0x03	// Kon'/Horse
#define BISHOP  0x04	// Slon/Bishop
#define QUEEN   0x05	// Ferzh'/Queen
#define KING	0x06	// Korol'/King

// Extract figure's type
#define FIGURE(x) (0x0F & x)

// Attributes reside in upper 4 bits
#define SET_BLACK(x) (x | 0x10)
#define IS_BLACK(x)  (0x10 & x)

#define SET_MOVED(x) (x | 0x20)
#define IS_MOVED(x)  (0x20 & x)

// For pawn en passant candidates
#define SET_PASSANT(x)   (x | 0x40)
#define CLEAR_PASSANT(x) (x & 0xbf)
#define IS_PASSANT(x)    (0x40 & x)

#define SET_PROMOTED(x)   (x | 0x80)
#define IS_PROMOTED(x)    (0x80 & x)
#define CLEAR_PROMOTED(x) (x & 0x7f)

#define WHITE 0x00
#define BLACK 0x10
#define TOGGLE_COLOR(x) (0x10 ^ x)
class Chessboard;

class Figure
{
protected:
    char m_figure_type;

    class ExceptionClass: public std::exception
    {
    private:
        std::string m_what_message;

    public:
        ExceptionClass(std::string msg):
             m_what_message(msg)
        {};
        const char* what()
        {
            return m_what_message.c_str();
        }
    };

public:
    Figure():
        m_figure_type('n')
    {};
    Figure(char type):
        m_figure_type(type)
    {};
    virtual ~Figure()
    {};

    virtual std::pair<std::vector<Move*>, std::vector<Move*>> get_moves(int position, Chessboard& board) = 0;

    char get_figure_type(){
        return this->m_figure_type;
    }

    void set_figure_type(char type){
        this->m_figure_type = type;
    }

    void check_color(int color){
        if (color != BLACK and color != WHITE)
        {
            std::stringstream flow;
            flow << "0x" << std::hex << color;
            throw ExceptionClass(std::string("Wrong Color with value: ") + std::string(flow.str()));
        }
    }
};

#endif
