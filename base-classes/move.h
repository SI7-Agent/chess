#ifndef MOVE_H
#define MOVE_H

#include <utility>

#include "figure.h"

class Move
{
protected:
    int m_figure;
    int m_target;
    int m_from, m_to;

public:
    Move():
        m_figure(0),
        m_target(0),
        m_from(0),
        m_to(0)
    {};
    void init(int moving, int from){
        this->m_figure = moving;
        this->m_from = from;
    }

    void set_figure(int figure){
        this->m_figure = figure;
    }

    int get_figure(){
        return this->m_figure;
    }

    void set_target(int target){
        this->m_target = target;
    }

    int get_target(){
        return this->m_target;
    }

    void set_from(int from){
        this->m_from = from;
    }

    int get_from(){
        return this->m_from;
    }

    void set_to(int to){
        this->m_to = to;
    }

    int get_to(){
        return this->m_to;
    }

    void set_direction(std::pair<int, int> direction){
        this->m_from = direction.first;
        this->m_to = direction.second;
    }

    std::pair<int, int> get_direction(){
        return std::pair<int, int>(this->m_from, this->m_to);
    }
};


#endif
