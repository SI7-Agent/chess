#ifndef PLAYER_H
#define PLAYER_H

#include "../chessboard/chessboard.h"

class Player
{
protected:
    char m_color;

public:
    Player():
        m_color('b')
    {};
    Player(char color):
        m_color(color)
    {};

    virtual ~Player()
    {};

    enum Status
    {
        Normal = 0,
        InCheck,
        Stalemate,
        Checkmate
    };

    virtual bool make_move(Chessboard& board, Move* &move) = 0;
};

#endif
