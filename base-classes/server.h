#ifndef SERVER_H
#define SERVER_H

#include <string>

class Server
{
protected:
    std::string m_ip;

public:
    Server():
        m_ip(std::string("localhost"))
    {};
    Server(std::string ip):
        m_ip(ip)
    {};
    void connect();
    void disconnect();
    void send(std::string state);
};

#endif
