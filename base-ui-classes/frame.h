#ifndef FRAME_H
#define FRAME_H

#include <wx/wx.h>

class Frame : public wxFrame
{
public:
    Frame(const wxString& title);
    void OnSize(wxSizeEvent& event);
};

#endif // FRAME_H
