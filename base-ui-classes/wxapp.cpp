#include "wxapp.h"
#include "../ui/boardframe.h"
#include "../ui/menuframe.h"

bool ImmortalChess::OnInit()
{
    wxInitAllImageHandlers();
    MenuFrame* frame = new MenuFrame(wxT("Menu"));
    frame->Show(true);

    return true;
}
