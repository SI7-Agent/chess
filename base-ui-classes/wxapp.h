#ifndef WXAPP_H
#define WXAPP_H

#include <wx/wx.h>

class ImmortalChess : public wxApp
{
public:
    virtual bool OnInit();
};

#endif // WXAPP_H
