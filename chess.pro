TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    base-ui-classes/frame.cpp \
    base-ui-classes/wxapp.cpp \
    chessboard/movemaking.cpp \
    figures/bishop.cpp \
    figures/empty.cpp \
    figures/knight.cpp \
    figures/pawn.cpp \
    figures/queen.cpp \
    figures/rook.cpp \
    chessboard/chessboardfen.cpp \
    chessboard/getIndices.cpp \
    chessboard/getPieceColorAt.cpp \
    figures/king.cpp \
    main.cpp \
    chessboard/chessboard.cpp \
    players/aiplayer.cpp \
    players/humanplayer.cpp \
    ui/boardframe.cpp \
    ui/icon.cpp \
    ui/menuframe.cpp \
    ui/panel.cpp \
    ui/transformdialog.cpp


wxCXXFLAGS = $$system(wx-config --cxxflags --unicode=yes --debug=no)
wxLinkOptions = $$system(wx-config --debug=no --libs --unicode=yes)
LIBS += $$wxLinkOptions
QMAKE_CXXFLAGS_RELEASE += $$wxCXXFLAGS
QMAKE_CXXFLAGS_DEBUG += $$wxCXXFLAGS

DISTFILES += \
    chess.pro.user

HEADERS += \
    base-classes/client.h \
    base-classes/figure.h \
    base-classes/move.h \
    base-classes/player.h \
    base-classes/server.h \
    base-ui-classes/frame.h \
    base-ui-classes/wxapp.h \
    chessboard/chessboard.h \
    figures/bishop.h \
    figures/empty.h \
    figures/king.h \
    figures/knight.h \
    figures/pawn.h \
    figures/queen.h \
    figures/rook.h \
    players/aiplayer.h \
    players/humanplayer.h \
    ui/boardframe.h \
    ui/icon.h \
    ui/menuframe.h \
    ui/panel.h \
    ui/transformdialog.h

