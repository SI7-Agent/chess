#include "chessboard.h"
#include "../figures/bishop.h"
#include "../figures/empty.h"
#include "../figures/knight.h"
#include "../figures/pawn.h"
#include "../figures/rook.h"
#include "../figures/king.h"
#include "../figures/queen.h"

std::vector<std::string> split(std::string str, char sep)
{
    std::vector<std::string> str_blocks;
    std::stringstream ss(str);
    std::string buffer;
    while(std::getline(ss, buffer, sep)){
        str_blocks.push_back(buffer);
    }
    return str_blocks;
}

std::pair<std::vector<Move*>, std::vector<Move*>> Chessboard::get_all_moves(int color)
{
    std::vector<Move*> regular, nulls;

    for (int i = 0; i < 64; i++)
    {
        int current_figure = m_figures[i]->get_figure_type();
        if ((current_figure != EMPTY) && (color == BLACK ? IS_BLACK(current_figure) : !IS_BLACK(current_figure))){
            std::pair<std::vector<Move*>, std::vector<Move*>> figures_moves = m_figures[i]->get_moves(i, *this);

            for (unsigned long i = 0; i < figures_moves.first.size(); i++)
                regular.push_back(figures_moves.first[i]);

            for (unsigned long i = 0; i < figures_moves.second.size(); i++)
                nulls.push_back(figures_moves.second[i]);
        }
    }

    return std::pair<std::vector<Move*>, std::vector<Move*>>(regular, nulls);
}

Figure* Chessboard::get_figure(int position)
{
    return m_figures[position];
}

void Chessboard::set_figure(Figure* what_to_set, int from_where_to_set, int where_to_set)
{
    free(m_figures[where_to_set]);
    m_figures[where_to_set] = what_to_set;

    m_figures[from_where_to_set] = new Empty();
}

void Chessboard::init()
{
    m_figures[A1] = new Rook(WHITE);
    m_figures[B1] = new Knight(WHITE);
    m_figures[C1] = new Bishop(WHITE);
    m_figures[D1] = new Queen(WHITE);
    m_figures[E1] = new King(WHITE);
    m_figures[F1] = new Bishop(WHITE);
    m_figures[G1] = new Knight(WHITE);
    m_figures[H1] = new Rook(WHITE);

    m_figures[A2] = new Pawn(WHITE);
    m_figures[B2] = new Pawn(WHITE);
    m_figures[C2] = new Pawn(WHITE);
    m_figures[D2] = new Pawn(WHITE);
    m_figures[E2] = new Pawn(WHITE);
    m_figures[F2] = new Pawn(WHITE);
    m_figures[G2] = new Pawn(WHITE);
    m_figures[H2] = new Pawn(WHITE);

    m_figures[A8] = new Rook(BLACK);
    m_figures[B8] = new Knight(BLACK);
    m_figures[C8] = new Bishop(BLACK);
    m_figures[D8] = new Queen(BLACK);
    m_figures[E8] = new King(BLACK);
    m_figures[F8] = new Bishop(BLACK);
    m_figures[G8] = new Knight(BLACK);
    m_figures[H8] = new Rook(BLACK);

    m_figures[A7] = new Pawn(BLACK);
    m_figures[B7] = new Pawn(BLACK);
    m_figures[C7] = new Pawn(BLACK);
    m_figures[D7] = new Pawn(BLACK);
    m_figures[E7] = new Pawn(BLACK);
    m_figures[F7] = new Pawn(BLACK);
    m_figures[G7] = new Pawn(BLACK);
    m_figures[H7] = new Pawn(BLACK);

    for (int i = A3; i < A7; i++)
        m_figures[i] = new Empty();

    m_black_king = E8;
    m_white_king = E1;

    m_active_color = WHITE;
}

Chessboard::Chessboard(Player* p1, Player* p2)
{
    m_active_color = WHITE;
    m_fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    m_figures = std::vector<Figure*>(m_length*m_length);
    m_player1 = p1;
    m_player2 = p2;
    m_pawn_transform = 0;
    init();
}

void Chessboard::set_figures(std::vector<Figure*> figures)
{
    m_figures = figures;
}

std::string Chessboard::getNotation(int i, int j)
{
    assert(0 <= i && i < m_length);
    assert(0 <= j && j < m_length);
    char file = 'a';
    char rank = '1';
    std::string notation;
    file += j;
    rank += m_length - 1 - i;
    notation += file;
    notation += rank;
    return notation;
}

std::string Chessboard::getDestination(std::string origin, int fileDirection, int rankDirection)
{
    char originFile = origin[0];
    char originRank = origin[1];
    std::string destination;
    char destinationFile = originFile + fileDirection;
    char destinationRank = originRank + rankDirection;

    if('a' <= destinationFile && destinationFile <= 'h'
    && '1' <= destinationRank && destinationRank <= '8'){
        destination += destinationFile;
        destination += destinationRank;
    } else {
        destination = "-";
    }
    return destination;
}

void Chessboard::changeActiveColor()
{
    m_active_color = TOGGLE_COLOR(m_active_color);
}

char Chessboard::abbr(int position)
{
    char return_abbr;
    char int_type = m_figures[position]->get_figure_type();

    switch(FIGURE(int_type))
    {
        case PAWN:
            IS_BLACK(int_type) ? (return_abbr = 'p') : (return_abbr = 'P');
            break;

        case ROOK:
            IS_BLACK(int_type) ? (return_abbr = 'r') : (return_abbr = 'R');
            break;

        case KNIGHT:
            IS_BLACK(int_type) ? (return_abbr = 'n') : (return_abbr = 'N');
            break;

        case BISHOP:
            IS_BLACK(int_type) ? (return_abbr = 'b') : (return_abbr = 'B');
            break;

        case KING:
            IS_BLACK(int_type) ? (return_abbr = 'k') : (return_abbr = 'K');
            break;

        case QUEEN:
            IS_BLACK(int_type) ? (return_abbr = 'q') : (return_abbr = 'Q');
            break;

        default:
            return_abbr = '-';
            break;
    }

    return return_abbr;
}

bool Chessboard::is_valid_move(Move* &move)
{
    bool valid = false;
    std::pair<std::vector<Move*>, std::vector<Move*>> moves = get_all_moves(m_active_color);

    for(std::vector<Move*>::iterator it = moves.first.begin(); it != moves.first.end() && !valid; ++it)
    {
        if(move->get_direction() == (*it)->get_direction())
        {
            move = *it;

            apply_move(move);
            King* king = (King*)get_figure(m_active_color ? m_black_king : m_white_king);

            if(!king->is_vulnerable(m_active_color ? m_black_king : m_white_king, *this))
                valid = true;

            undo_move(move);
            break;
        }
    }

    return valid;
}

int Chessboard::get_status()
{
    bool king_vulnerable = false, can_move = false;

    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves = get_all_moves(m_active_color);

    if(((King*)get_figure(m_active_color ? m_black_king : m_white_king))->is_vulnerable(m_active_color ? m_black_king : m_white_king, *this))
        king_vulnerable = true;

    for(std::vector<Move*>::iterator it = all_moves.first.begin(); it != all_moves.first.end() && !can_move; ++it)
    {
        apply_move(*it);
        if(!((King*)get_figure(m_active_color ? m_black_king : m_white_king))->is_vulnerable(m_active_color ? m_black_king : m_white_king, *this))
        {
            can_move = true;
        }
        undo_move(*it);
    }

    if(king_vulnerable && can_move)
        return Player::InCheck;
    if(king_vulnerable && !can_move)
        return Player::Checkmate;
    if(!king_vulnerable && !can_move)
        return Player::Stalemate;

    return Player::Normal;
}
