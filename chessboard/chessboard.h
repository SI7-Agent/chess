#ifndef BOARD_H
#define BOARD_H

#include <algorithm>
#include <cassert>
#include <complex>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "../base-classes/figure.h"
#include "../base-classes/player.h"

class Player;

class Chessboard
{
public:
    Chessboard(Player* p1, Player* p2);
    void set_figures(std::vector<Figure*> figures);
    void init();

    void FEN2Board();
    void FEN2Board(std::string fen);
    void board2FEN();

    std::pair<std::vector<Move*>, std::vector<Move*>> get_all_moves(int color);
    Figure* get_figure(int position);
    void set_figure(Figure* what_to_set, int from_where_to_set, int where_to_set);

    bool is_valid_move(Move* &move);
    void apply_move(Move* move);
    void apply_king_move(Move* move);
    void apply_pawn_move(Move* move);

    void undo_move(Move* move);
    void undo_king_move(Move* move);
    void undo_pawn_move(Move* move);

    static Figure* pawn_transform(int type, int color);
    static Figure* previous_figure(char code);

    int get_status();

    std::vector<int> getIndices(std::string notation);
    std::string getNotation(int i, int j);
    char getPieceColorAt(int i);
    char abbr(int position);
    bool move(std::string origin, std::string destination);
    std::string getDestination(std::string origin, int fileDirection, int rankDirection);
    std::vector<std::string> getAttackingSquares(char color);
    std::string getFEN() { return m_fen; }
    char getActiveColor() { return m_active_color; }
    std::string getCastlability() { return castlability; }
    int getHalfmoveClock() { return halfmoveClock; }
    int getFullmoveNumber() { return fullmoveNumber; }
    std::string enPassantTarget;
    std::string castlability;
    int halfmoveClock;
    int fullmoveNumber;
    bool isFinished() { return m_finished; }
    std::string getResult() { return m_result; }
    void changeActiveColor();
    void set_pawn_transform(int p) { m_pawn_transform = p; }
    int get_pawn_transform() { return m_pawn_transform; }
    char getKingPosition(char color) { return color ? m_black_king : m_white_king; }
    bool inCheck(char color);
    void print();
    static const int m_length = 8;

    Player* m_player1;
    Player* m_player2;

private:
    enum m_position {
            A1 = 0, B1, C1, D1, E1, F1, G1, H1,
            A2, B2, C2, D2, E2, F2, G2, H2,
            A3, B3, C3, D3, E3, F3, G3, H3,
            A4, B4, C4, D4, E4, F4, G4, H4,
            A5, B5, C5, D5, E5, F5, G5, H5,
            A6, B6, C6, D6, E6, F6, G6, H6,
            A7, B7, C7, D7, E7, F7, G7, H7,
            A8, B8, C8, D8, E8, F8, G8, H8
        };
    bool m_finished;
    std::string m_result;
    std::string m_fen;
    char m_active_color;
    std::vector<Figure*> m_figures;
    int m_black_king, m_white_king;
    int m_pawn_transform;
};

std::vector<std::string> split(std::string str, char sep);

#endif
