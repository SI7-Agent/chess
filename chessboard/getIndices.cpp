#include "chessboard.h"

std::vector<int> Chessboard::getIndices(std::string notation)
{
    assert(notation.size() == 2);
    assert('a' <= notation[0] && notation[0] <= 'h');
    assert('1' <= notation[1] && notation[1] <= '8');
    std::vector<int> indices;
    indices.push_back(7 - (notation[1] - '1'));
    indices.push_back(notation[0] - 'a');
    return indices;
}
