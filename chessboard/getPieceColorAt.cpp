#include "chessboard.h"

char Chessboard::getPieceColorAt(int i)
{
    return IS_BLACK(m_figures[i]->get_figure_type()) ? BLACK : WHITE;
}
