#include "chessboard.h"
#include "../figures/bishop.h"
#include "../figures/empty.h"
#include "../figures/knight.h"
#include "../figures/pawn.h"
#include "../figures/rook.h"
#include "../figures/king.h"
#include "../figures/queen.h"

void Chessboard::apply_move(Move* move)
{
    char figure_code = move->get_figure();
    switch(FIGURE(figure_code))
    {
        case KING:
            apply_king_move(move);
            break;
        case PAWN:
            if (move->get_from() != move->get_to())
            {
                apply_pawn_move(move);
                break;
            }
        default:
            this->set_figure(m_figures[move->get_from()], move->get_from(), move->get_to());
            m_figures[move->get_to()]->set_figure_type(SET_MOVED(figure_code));
            break;
    }
}

void Chessboard::apply_king_move(Move* move)
{
    char king_figure = move->get_figure();
    if(!IS_MOVED(king_figure))
    {
        switch(move->get_to())
        {
            case G1:
                m_figures[H1]->set_figure_type(SET_MOVED(m_figures[H1]->get_figure_type()));
                this->set_figure(m_figures[H1], H1, F1);
                break;
            case G8:
                m_figures[H8]->set_figure_type(SET_MOVED(m_figures[H8]->get_figure_type()));
                this->set_figure(m_figures[H8], H8, F8);
                break;
            case C1:
                m_figures[A1]->set_figure_type(SET_MOVED(m_figures[A1]->get_figure_type()));
                this->set_figure(m_figures[A1], A1, D1);
                break;
            case C8:
                m_figures[A8]->set_figure_type(SET_MOVED(m_figures[A8]->get_figure_type()));
                this->set_figure(m_figures[A8], A8, D8);
                break;
            default:
                break;
        }
    }

    m_figures[move->get_from()]->set_figure_type(SET_MOVED(king_figure));
    this->set_figure(m_figures[move->get_from()], move->get_from(), move->get_to());

    IS_BLACK(king_figure) ? (m_black_king = move->get_to()) : (m_white_king = move->get_to());
}

void Chessboard::apply_pawn_move(Move *move)
{
    int capture_field;

    if(IS_PASSANT(move->get_target()))
    {
        if(IS_BLACK(move->get_figure()))
        {
            capture_field = move->get_to() + 8;
            if((move->get_from() / 8) == 3)
            {
                free(m_figures[capture_field]);
                m_figures[capture_field] = new Empty();
            }
        }
        else
        {
            capture_field = move->get_to() - 8;
            if((move->get_from() / 8) == 4)
            {
                free(m_figures[capture_field]);
                m_figures[capture_field] = new Empty();
            }
        }
    }

    if(IS_BLACK(move->get_figure()))
    {
        if(move->get_to() / 8 == 0)
        {
            free(m_figures[move->get_to()]);
            m_figures[move->get_to()] = pawn_transform(m_pawn_transform, BLACK);
        }
        else
        {
            free(m_figures[move->get_to()]);
            m_figures[move->get_to()] = new Pawn();
            m_figures[move->get_to()]->set_figure_type(move->get_figure());
        }
    }
    else
    {
        if(move->get_to() / 8 == 7)
        {
            free(m_figures[move->get_to()]);
            m_figures[move->get_to()] = pawn_transform(m_pawn_transform, WHITE);
        }
        else
        {
            free(m_figures[move->get_to()]);
            m_figures[move->get_to()] = new Pawn();
            m_figures[move->get_to()]->set_figure_type(move->get_figure());
        }
    }

    m_figures[move->get_from()] = new Empty();
}

Figure* Chessboard::pawn_transform(int type, int color)
{
    Figure* promoted;

    switch(type)
    {
        case 0:
            promoted = new Queen(color);
            break;
        case 1:
            promoted = new Rook(color);
            break;
        case 2:
            promoted = new Knight(color);
            break;
        case 3:
            promoted = new Bishop(color);
            break;
        default:
            break;
    }

    promoted->set_figure_type(SET_MOVED(promoted->get_figure_type()));

    return promoted;
}

void Chessboard::undo_move(Move *move)
{
    char figure_code = move->get_figure();
    switch(FIGURE(figure_code))
    {
        case KING:
            undo_king_move(move);
            break;
        case PAWN:
            if (move->get_from() != move->get_to())
            {
                undo_pawn_move(move);
                break;
            }
        default:
            free(m_figures[move->get_from()]);
            m_figures[move->get_from()] = m_figures[move->get_to()];

            m_figures[move->get_from()]->set_figure_type(move->get_figure());
            m_figures[move->get_to()] = previous_figure(move->get_target());
            break;
    }
}

void Chessboard::undo_king_move(Move *move)
{
    char king_figure = move->get_figure();
    if(!IS_MOVED(king_figure))
    {
        switch(move->get_to())
        {
            case G1:
                free(m_figures[H1]);
                m_figures[H1] = m_figures[F1];

                m_figures[H1]->set_figure_type(ROOK);
                m_figures[F1] = previous_figure(EMPTY);
                break;
            case G8:
                free(m_figures[H8]);
                m_figures[H8] = m_figures[F8];

                m_figures[H8]->set_figure_type(SET_BLACK(ROOK));
                m_figures[F8] = previous_figure(EMPTY);
                break;
            case C1:
                free(m_figures[A1]);
                m_figures[A1] = m_figures[D1];

                m_figures[A1]->set_figure_type(ROOK);
                m_figures[D1] = previous_figure(EMPTY);
                break;
            case C8:
                free(m_figures[A8]);
                m_figures[A8] = m_figures[D8];

                m_figures[A8]->set_figure_type(SET_BLACK(ROOK));
                m_figures[D8] = previous_figure(EMPTY);
                break;
            default:
                break;
        }
    }

    free(m_figures[move->get_from()]);
    m_figures[move->get_from()] = m_figures[move->get_to()];

    m_figures[move->get_from()]->set_figure_type(move->get_figure());
    m_figures[move->get_to()] = previous_figure(move->get_target());

    IS_BLACK(king_figure) ? (m_black_king = move->get_from()) : (m_white_king = move->get_from());
}

void Chessboard::undo_pawn_move(Move *move)
{
    int capture_field;

    free(m_figures[move->get_from()]);
    m_figures[move->get_from()] = previous_figure(CLEAR_PASSANT(move->get_figure()));
    if(move->get_from()/8 == 1 or move->get_from() == 6)
    {
        if(m_active_color)
            m_figures[move->get_from()]->set_figure_type(PAWN);
        else
            m_figures[move->get_from()]->set_figure_type(SET_BLACK(PAWN));
    }

    if(IS_PASSANT(move->get_target()))
    {
        if(IS_BLACK(move->get_figure()))
        {
            capture_field = move->get_to() + 8;
            if(move->get_from() / 8 == 3)
            {
                free(m_figures[capture_field]);
                m_figures[capture_field] = previous_figure(move->get_target());

                m_figures[move->get_to()] = new Empty();
            }
            else
            {
                free(m_figures[move->get_to()]);
                m_figures[move->get_to()] = previous_figure(move->get_target());
            }
        }
        else
        {
            capture_field = move->get_to() - 8;
            if(move->get_from() / 8 == 4)
            {
                free(m_figures[capture_field]);
                m_figures[capture_field] = previous_figure(move->get_target());

                m_figures[move->get_to()] = new Empty();
            }
            else
            {
                free(m_figures[move->get_to()]);
                m_figures[move->get_to()] = previous_figure(move->get_target());
            }
        }
    }
    else
    {
        free(m_figures[move->get_to()]);
        m_figures[move->get_to()] = previous_figure(move->get_target());
    }
}

Figure* Chessboard::previous_figure(char code)
{
    Figure* previous;
    switch(FIGURE(code))
    {
        case PAWN:
            previous = new Pawn();
            break;

        case ROOK:
            previous = new Rook();
            break;

        case KNIGHT:
            previous = new Knight();
            break;

        case BISHOP:
            previous = new Bishop();
            break;

        case KING:
            previous = new King();
            break;

        case QUEEN:
            previous = new Queen();
            break;

        default:
            previous = new Empty();
            break;
    }

    previous->set_figure_type(code);

    return previous;
}
