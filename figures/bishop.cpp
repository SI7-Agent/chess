#include "bishop.h"

Bishop::Bishop()
{
    this->m_figure_type = BISHOP;
}

Bishop::Bishop(int color)
{
    try {
        this->check_color(color);

        if (color == BLACK)
            this->m_figure_type = SET_BLACK(BISHOP);
        else
            this->m_figure_type = BISHOP;
    } catch (Figure::ExceptionClass& ex) {
        std::cout << ex.what();
    }
}

std::pair<std::vector<Move*>, std::vector<Move*>> Bishop::get_moves(int position, Chessboard& board)
{
    std::vector<Move*> regulars, nulls;
    Move* new_move;
    int destination_from = position;
    int destination_to = destination_from;
    int target;

    int current_row, current_column, row, column;
    char current_figure = this->get_figure_type();

    current_row = position / 8;
    current_column = position % 8;

    // step 1. up-right diagonal
    for(row = current_row + 1, column = current_column + 1; (row < 8) && (column < 8); row++, column++)
    {
        destination_to = row * 8 + column;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }

            break;
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_target(target);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            regulars.push_back(new_move);

        }
    }

    // step 2. down-left diagonal
    for(row = current_row - 1, column = current_column - 1; (row >= 0) && (column >= 0); row--, column--)
    {
        destination_to = row * 8 + column;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }

            break;
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_target(target);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            regulars.push_back(new_move);

        }
    }

    // step 3. up-left diagonal
    for(row = current_row + 1, column = current_column - 1; (row < 8) && (column >= 0); row++, column--)
    {
        destination_to = row * 8 + column;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }

            break;
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_target(target);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            regulars.push_back(new_move);

        }
    }

    // step 4. down-right diagonal
    for(row = current_row - 1, column = current_column + 1; (row >= 0) && (column < 8); row--, column++)
    {
        destination_to = row * 8 + column;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }

            break;
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_target(target);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            regulars.push_back(new_move);

        }
    }

    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves;
    all_moves.first = regulars;
    all_moves.second = nulls;

    return all_moves;
}

Bishop::~Bishop()
{

}
