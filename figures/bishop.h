#ifndef BISHOP_H
#define BISHOP_H

#include "../chessboard/chessboard.h"
#include "../base-classes/figure.h"

class Bishop : public Figure
{
public:
    Bishop();
    Bishop(int color);
    ~Bishop();

    std::pair<std::vector<Move*>, std::vector<Move*>> get_moves(int position, Chessboard& board);
};

#endif // BISHOP_H
