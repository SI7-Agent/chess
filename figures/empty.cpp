#include "empty.h"

Empty::Empty()
{
    this->m_figure_type = EMPTY;
}

std::pair<std::vector<Move*>, std::vector<Move*>> Empty::get_moves(int position, Chessboard& board)
{
    std::vector<Move*> regulars, nulls;

    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves;
    all_moves.first = regulars;
    all_moves.second = nulls;

    return all_moves;
}

Empty::~Empty()
{

}
