#ifndef EMPTY_H
#define EMPTY_H

#include "../chessboard/chessboard.h"
#include "../base-classes/figure.h"

class Empty: public Figure
{
public:
    Empty();
    ~Empty();

    std::pair<std::vector<Move*>, std::vector<Move*>> get_moves(int position, Chessboard& board);

};

#endif // EMPTY_H
