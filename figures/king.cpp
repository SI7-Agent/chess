﻿#include "king.h"

King::King()
{
    this->m_figure_type = KING;
}

King::King(int color)
{
    try {
        this->check_color(color);

        if (color == BLACK)
            this->m_figure_type = SET_BLACK(KING);
        else
            this->m_figure_type = KING;
    }  catch (Figure::ExceptionClass& ex) {
        std::cout << ex.what();
    }
}

std::pair<std::vector<Move*>, std::vector<Move*>> King::get_moves(int position, Chessboard& board)
{
    std::vector<Move*> regulars, nulls;
    Move* new_move;
    int destination_from = position;
    int destination_to = destination_from;
    int target;

    int current_row, current_column;
    char current_figure = this->get_figure_type();

    current_row = position / 8;
    current_column = position % 8;

    // Determine row and column
    current_row = position / 8;
    current_column = position % 8;

    // step 1. Move left
    if(current_column > 0)
    {
        // step 1.1 up
        if(current_row < 7)
        {
            destination_to = position + 7;
            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(target) != IS_BLACK(current_figure))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    new_move->set_target(target);
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);
                regulars.push_back(new_move);
            }
        }

        // step 1.2 middle

        destination_to = position - 1;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(target) != IS_BLACK(current_figure))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);
                regulars.push_back(new_move);
            }
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            new_move->set_target(target);
            regulars.push_back(new_move);
        }

        // step 1.3 down
        if(current_row > 0)
        {
            destination_to = position - 9;
            if((target = board.get_figure(destination_to)->get_figure_type())!= EMPTY)
            {
                if(IS_BLACK(target) != IS_BLACK(current_figure))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    new_move->set_target(target);
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);
                regulars.push_back(new_move);
            }
        }
    }

    // step 2. Move right
    if(current_column < 7)
    {
        // step 2.1 up
        if(current_row < 7)
        {
            destination_to = position + 9;
            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(target) != IS_BLACK(current_figure))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    new_move->set_target(target);
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);
                regulars.push_back(new_move);
            }
        }

        // step 2.2 middle
        destination_to = position + 1;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(target) != IS_BLACK(current_figure))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);
                regulars.push_back(new_move);
            }
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            new_move->set_target(target);
            regulars.push_back(new_move);
        }

        // step 2.3 down
        if(current_row > 0)
        {
            destination_to = position - 7;
            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(target) != IS_BLACK(current_figure))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    new_move->set_target(target);
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);

                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);
                regulars.push_back(new_move);
            }
        }
    }

    // step 3. straight up
    if(current_row < 7)
    {
        // step 2.2 middle
        destination_to = position + 8;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(target) != IS_BLACK(current_figure))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);
                regulars.push_back(new_move);
            }
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            new_move->set_target(target);
            regulars.push_back(new_move);
        }
    }

    // step 4. straight down
    if(current_row > 0)
    {
        // step 2.2 middle
        destination_to = position - 8;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(target) != IS_BLACK(current_figure))
            {

                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                new_move->set_target(target);
                regulars.push_back(new_move);
            }
        }
        else
        {

            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            new_move->set_target(target);
            regulars.push_back(new_move);
        }
    }

    // step 5. Castling
    if(!IS_MOVED(current_figure) && !is_vulnerable(position, board))
    {
        // short
        destination_to = IS_BLACK(current_figure) ? 61 : 5;
        if((board.get_figure(destination_to)->get_figure_type() == EMPTY) && !is_vulnerable(destination_to, board))
        {
            destination_to = IS_BLACK(current_figure) ? 62 : 6;
            if((board.get_figure(destination_to)->get_figure_type() == EMPTY) && !is_vulnerable(destination_to, board))
            {
                destination_to = IS_BLACK(current_figure) ? 63 : 7;
                target = board.get_figure(destination_to)->get_figure_type();
                if(!IS_MOVED(target) && (FIGURE(target) == ROOK))
                {
                    if(IS_BLACK(target) == IS_BLACK(current_figure))
                    {
                        new_move = new Move();
                        new_move->set_figure(current_figure);
                        new_move->set_direction(std::pair<int, int>(destination_from, IS_BLACK(current_figure) ? 62 : 6));
                        new_move->set_target(EMPTY);
                        regulars.push_back(new_move);
                    }
                }
            }
        }

        // long
        destination_to = IS_BLACK(current_figure) ? 59 : 3;
        if((board.get_figure(destination_to)->get_figure_type() == EMPTY) && !is_vulnerable(destination_to, board))
        {
            destination_to = IS_BLACK(current_figure) ? 58 : 2;
            if((board.get_figure(destination_to)->get_figure_type() == EMPTY) && !is_vulnerable(destination_to, board))
            {
                destination_to = IS_BLACK(current_figure) ? 56 : 0;
                target = board.get_figure(destination_to)->get_figure_type();
                if(!IS_MOVED(target) && (FIGURE(target) == ROOK))
                {
                    if(IS_BLACK(target) == IS_BLACK(current_figure))
                    {
                        new_move = new Move();
                        new_move->set_figure(current_figure);
                        new_move->set_direction(std::pair<int, int>(destination_from, IS_BLACK(current_figure) ? 58 : 2));
                        new_move->set_target(EMPTY);
                        regulars.push_back(new_move);
                    }
                }
            }
        }
    }

    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves;
    all_moves.first = regulars;
    all_moves.second = nulls;

    return all_moves;
}

bool King::is_vulnerable(int position, Chessboard& board)
{
    int destination_to, target, current_row, current_column, i,j, end;
    char current_figure = this->get_figure_type();

    // Determine row and column
    current_row = position / 8;
    current_column = position % 8;

    // step 1. Look for Rooks, Queens and Kings above
    for(destination_to = position + 8; destination_to < 64; destination_to += 8)
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(target) != IS_BLACK(current_figure))
            {
                if((destination_to - position) == 8)
                {
                    if(FIGURE(target) == KING)
                        return true;
                }

                if((FIGURE(target) == ROOK) || (FIGURE(target) == QUEEN))
                    return true;
            }

            break;
        }
    }

    // step 2. Look for Rooks, Queens and Kings below
    for(destination_to = position - 8; destination_to >= 0; destination_to -= 8)
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(target) != IS_BLACK(current_figure))
            {
                if((position - destination_to) == 8)
                {
                    if(FIGURE(target) == KING)
                        return true;
                }

                if((FIGURE(target) == ROOK) || (FIGURE(target) == QUEEN))
                    return true;
            }

            break;
        }
    }

    // step 3. Look for Rooks, Queens and Kings left
    for(destination_to = position - 1, end = position - (position % 8); destination_to >= end; destination_to--)
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                if((position - destination_to) == 1)
                {
                    if(FIGURE(target) == KING)
                        return true;
                }

                if((FIGURE(target) == ROOK) || (FIGURE(target) == QUEEN))
                    return true;
            }

            break;
        }
    }

    // step 4. Look for Rooks, Queens and Kings right
    for(destination_to = position + 1, end = position + (8 - position % 8); destination_to < end; destination_to++)
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                if((destination_to - position) == 1)
                {
                    if(FIGURE(target) == KING)
                        return true;
                }

                if((FIGURE(target) == ROOK) || (FIGURE(target) == QUEEN))
                    return true;
            }

            break;
        }
    }

    // step 5. Look for Bishops, Queens, Kings and Pawns north-east
    for(i = current_row + 1, j = current_column + 1; (i < 8) && (j < 8); i++, j++)
    {
        destination_to = i * 8 + j;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                if((destination_to - position) == 9)
                {
                    if(FIGURE(target) == KING)
                        return true;
                    else if(!IS_BLACK(current_figure) && (FIGURE(target) == PAWN))
                        return true;
                }

                if((FIGURE(target) == BISHOP) || (FIGURE(target) == QUEEN))
                    return true;
            }

            break;
        }
    }

    // step 6. Look for Bishops, Queens, Kings and Pawns south-east
    for(i = current_row - 1, j = current_column + 1; (i >= 0) && (j < 8); i--, j++)
    {
        destination_to = i * 8 + j;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                if((position - destination_to) == 7)
                {
                    if(FIGURE(target) == KING)
                        return true;
                    else if(IS_BLACK(current_figure) && (FIGURE(target) == PAWN))
                        return true;
                }

                if((FIGURE(target) == BISHOP) || (FIGURE(target) == QUEEN))
                    return true;
            }

            break;
        }
    }

    // step 7. Look for Bishops, Queens, Kings and Pawns south-west
    for(i = current_row - 1, j = current_column - 1; (i >= 0) && (j >= 0); i--, j--)
    {
        destination_to = i * 8 + j;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                if((position - destination_to) == 9)
                {
                    if(FIGURE(target) == KING)
                        return true;
                    else if(IS_BLACK(current_figure) && (FIGURE(target) == PAWN))
                        return true;
                }

                if((FIGURE(target) == BISHOP) || (FIGURE(target) == QUEEN))
                    return true;
            }

            break;
        }
    }

    // step 8. Look for Bishops, Queens, Kings and Pawns north-west
    for(i = current_row + 1, j = current_column - 1; (i < 8) && (j >= 0); i++, j--)
    {
        destination_to = i * 8 + j;
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                if((destination_to - position) == 7)
                {
                    if(FIGURE(target) == KING)
                        return true;
                    else if(!IS_BLACK(current_figure) && (FIGURE(target) == PAWN))
                        return true;
                }

                if((FIGURE(target) == BISHOP) || (FIGURE(target) == QUEEN))
                    return true;
            }

            break;
        }
    }

    // step 9. Look for Knights in upper positionitions
    if(current_row < 6)
    {
        // right
        if(current_column < 7)
        {
            destination_to = position + 17;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    if(FIGURE(target) == KNIGHT) return true;
                }
            }
        }

        // left
        if(current_column > 0)
        {
            destination_to = position + 15;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    if(FIGURE(target) == KNIGHT) return true;
                }
            }
        }
    }

    // step 10. Look for Knights in lower positionitions
    if(current_row > 1)
    {
        // right
        if(current_column < 7)
        {
            destination_to = position - 15;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    if(FIGURE(target) == KNIGHT) return true;
                }
            }
        }

        // left
        if(current_column > 0)
        {
            destination_to = position - 17;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    if(FIGURE(target) == KNIGHT) return true;
                }
            }
        }
    }

    // step 11. Look for Knights in right positionitions
    if(current_column < 6)
    {
        // up
        if(current_row < 7)
        {
            destination_to = position + 10;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    if(FIGURE(target) == KNIGHT) return true;
                }
            }
        }

        // down
        if(current_row > 0)
        {
            destination_to = position - 6;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    if(FIGURE(target) == KNIGHT) return true;
                }
            }
        }
    }

    // step 12. Look for Knights in left positionitions
    if(current_column > 1)
    {
        // up
        if(current_row < 7)
        {
            destination_to = position + 6;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    if(FIGURE(target) == KNIGHT) return true;
                }
            }
        }

        // down
        if(current_row > 0)
        {
            destination_to = position - 10;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    if(FIGURE(target) == KNIGHT) return true;
                }
            }
        }
    }

    return false;
}

King::~King()
{

}
