#ifndef KING_H
#define KING_H

#include "../chessboard/chessboard.h"
#include "../base-classes/figure.h"

#include <iostream>

class King : public Figure
{
public:
    King();
    King(int color);
    ~King();

    std::pair<std::vector<Move*>, std::vector<Move*>> get_moves(int position, Chessboard& board);
    bool is_vulnerable(int position, Chessboard& board);
};

#endif // KING_H
