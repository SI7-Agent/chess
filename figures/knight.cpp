#include "knight.h"

Knight::Knight()
{
    this->m_figure_type = KNIGHT;
}

Knight::Knight(int color)
{
    try {
        this->check_color(color);

        if (color == BLACK)
            this->m_figure_type = SET_BLACK(KNIGHT);
        else
            this->m_figure_type = KNIGHT;
    } catch (Figure::ExceptionClass& ex) {
        std::cout << ex.what();
    }
}

std::pair<std::vector<Move*>, std::vector<Move*>> Knight::get_moves(int position, Chessboard& board)
{
    std::vector<Move*> regulars, nulls;
    Move* new_move;
    int destination_from = position;
    int destination_to = destination_from;
    int target;

    int current_row, current_column;
    char current_figure = this->get_figure_type();

    current_row = position / 8;
    current_column = position % 8;

    // step 1. two cells up
    if(current_row < 6)
    {
        // step 1.1. right
        if(current_column < 7)
        {
            destination_to = position + 17;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_target(target);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }
        }

        // step 1.2. left
        if(current_column > 0)
        {
            destination_to = position + 15;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_target(target);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }
        }
    }

    // step 2. two cells down
    if(current_row > 1)
    {
        // step 2.1. right
        if(current_column < 7)
        {
            destination_to = position - 15;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_target(target);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }
        }

        // step 2.2. left
        if(current_column > 0)
        {
            destination_to = position - 17;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_target(target);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }
        }
    }

    // step 3. two cells right
    if(current_column < 6)
    {
        // step 3.1. up
        if(current_row < 7)
        {
            destination_to = position + 10;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_target(target);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }
        }

        // step 3.2. down
        if(current_row > 0)
        {
            destination_to = position - 6;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_target(target);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }
        }
    }

    // step 4. two cells left
    if(current_column > 1)
    {
        // step 4.1. up
        if(current_row < 7)
        {
            destination_to = position + 6;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_target(target);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }
        }

        // step 4.2. down
        if(current_row > 0)
        {
            destination_to = position - 10;

            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(current_figure) != IS_BLACK(target))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_target(target);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    regulars.push_back(new_move);
                }
            }
            else
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }
        }
    }

    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves;
    all_moves.first = regulars;
    all_moves.second = nulls;

    return all_moves;
}

Knight::~Knight()
{

}
