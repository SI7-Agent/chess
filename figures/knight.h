#ifndef KNIGHT_H
#define KNIGHT_H

#include "../chessboard/chessboard.h"
#include "../base-classes/figure.h"

class Knight : public Figure
{
public:
    Knight();
    Knight(int color);
    ~Knight();

    std::pair<std::vector<Move*>, std::vector<Move*>> get_moves(int position, Chessboard& board);
};

#endif // KNIGHT_H
