#include "pawn.h"

Pawn::Pawn()
{
    this->m_figure_type = PAWN;
}

Pawn::Pawn(int color)
{
    try {
        this->check_color(color);

        if (color == BLACK)
            this->m_figure_type = SET_BLACK(PAWN);
        else
            this->m_figure_type = PAWN;
    }  catch (Figure::ExceptionClass& ex) {
        std::cout << ex.what();
    }
}

std::pair<std::vector<Move*>, std::vector<Move*>> Pawn::get_moves(int position, Chessboard& board){
    std::vector<Move*> regulars, nulls;
    Move* new_move;
    int destination_from = position;
    int destination_to = destination_from;
    int target;
    char current_figure = this->get_figure_type();

    // step 1. check here for en passant moves - we need to count them even if player don't make such moves
    if (IS_PASSANT(current_figure)){
        new_move = new Move();
        new_move->set_figure(CLEAR_PASSANT(current_figure));
        new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
        new_move->set_target(current_figure);
        nulls.push_back(new_move);

        current_figure = CLEAR_PASSANT(current_figure);
    }

    // step 2. making only 1 cell move
    destination_to = IS_BLACK(current_figure) ? position - 8 : position + 8;
    if((destination_to >= 0) && (destination_to < 64))
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) == EMPTY)
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            new_move->set_target(target);
            regulars.push_back(new_move);

            // step 3. making 2 cell move
            if(!IS_MOVED(current_figure))
            {
                destination_to = IS_BLACK(current_figure) ? position - 16 : position + 16;
                if((destination_to >= 0) && (destination_to < 64))
                {
                    if((target = board.get_figure(destination_to)->get_figure_type()) == EMPTY)
                    {
                        new_move = new Move();
                        new_move->set_figure(SET_PASSANT(SET_MOVED(current_figure)));
                        new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                        new_move->set_target(target);
                        regulars.push_back(new_move);
                    }
                }
            }
        }
    }

    // step 4. takedown white - left, black - right
    if(position % 8 != 0)
    {
        destination_to = IS_BLACK(current_figure) ? position - 9 : position + 7;
        if((destination_to >= 0) && (destination_to < 64))
        {
            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                int a = IS_BLACK(target);
                int b = IS_BLACK(current_figure);
                if(IS_BLACK(target) != IS_BLACK(current_figure))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    new_move->set_target(target);
                    regulars.push_back(new_move);
                }
            }
            else
            {
                target = board.get_figure(position - 1)->get_figure_type();
                if(IS_PASSANT(target))
                {
                    if(IS_BLACK(target) != IS_BLACK(current_figure))
                    {
                        new_move = new Move();
                        new_move->set_figure(current_figure);
                        new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                        new_move->set_target(target);
                        regulars.push_back(new_move);
                    }
                }
            }
        }
    }

    //step 5. takedown black - left, white - right
    if(position % 8 != 7)
    {
        destination_to = IS_BLACK(current_figure) ? position - 7 : position + 9;
        if((destination_to >= 0) && (destination_to < 64))
        {
            if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
            {
                if(IS_BLACK(target) != IS_BLACK(current_figure))
                {
                    new_move = new Move();
                    new_move->set_figure(current_figure);
                    new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                    new_move->set_target(target);
                    regulars.push_back(new_move);
                }
            }
            else
            {
                target = board.get_figure(position + 1)->get_figure_type();
                if(IS_PASSANT(target))
                {
                    if(IS_BLACK(target) != IS_BLACK(current_figure))
                    {
                        new_move = new Move();
                        new_move->set_figure(current_figure);
                        new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                        new_move->set_target(target);
                        regulars.push_back(new_move);
                    }
                }
            }
        }
    }

    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves;
    all_moves.first = regulars;
    all_moves.second = nulls;

    return all_moves;
}

Pawn::~Pawn()
{

}
