#ifndef PAWN_H
#define PAWN_H

#include "../chessboard/chessboard.h"
#include "../base-classes/figure.h"

#include <iostream>


class Pawn: public Figure
{
public:
    Pawn();
    Pawn(int color);
    ~Pawn();

    std::pair<std::vector<Move*>, std::vector<Move*>> get_moves(int position, Chessboard& board);
};

#endif
