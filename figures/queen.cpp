#include "queen.h"
#include "rook.h"
#include "bishop.h"

Queen::Queen()
{
    this->m_figure_type = QUEEN;
}

Queen::Queen(int color)
{
    try {
        this->check_color(color);

        if (color == BLACK)
            this->m_figure_type = SET_BLACK(QUEEN);
        else
            this->m_figure_type = QUEEN;
    } catch (Figure::ExceptionClass& ex) {
        std::cout << ex.what();
    }
}

std::pair<std::vector<Move*>, std::vector<Move*>> Queen::get_moves(int position, Chessboard& board)
{
    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves;
    std::vector<Move*> regular, nulls;

    int current_figure = get_figure_type();

    Figure* new_rook = new Rook();
    new_rook->set_figure_type(current_figure);

    Figure* new_bishop = new Bishop();
    new_bishop->set_figure_type(current_figure);

    all_moves = new_rook->get_moves(position, board);
    std::pair<std::vector<Move*>, std::vector<Move*>> figures_moves = new_bishop->get_moves(position, board);

    for (unsigned long i = 0; i < figures_moves.first.size(); i++)
        all_moves.first.push_back(figures_moves.first[i]);

    for (unsigned long i = 0; i < figures_moves.second.size(); i++)
        all_moves.second.push_back(figures_moves.second[i]);


    return all_moves;
}

Queen::~Queen()
{

}
