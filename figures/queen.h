#ifndef QUEEN_H
#define QUEEN_H

#include "../chessboard/chessboard.h"
#include "../base-classes/figure.h"

class Queen : public Figure
{
public:
    Queen();
    Queen(int color);
    ~Queen();

    std::pair<std::vector<Move*>, std::vector<Move*>> get_moves(int position, Chessboard& board);
};

#endif // QUEEN_H
