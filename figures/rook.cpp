#include "rook.h"

Rook::Rook()
{
    this->m_figure_type = ROOK;
}

Rook::Rook(int color)
{
    try {
        this->check_color(color);

        if (color == BLACK)
            this->m_figure_type = SET_BLACK(ROOK);
        else
            this->m_figure_type = ROOK;
    } catch (Figure::ExceptionClass& ex) {
        std::cout << ex.what();
    }
}

std::pair<std::vector<Move*>, std::vector<Move*>> Rook::get_moves(int position, Chessboard& board)
{
    std::vector<Move*> regulars, nulls;
    Move* new_move;
    int destination_from = position;
    int destination_to = destination_from;
    int target;
    int end_left_move = position - (position % 8);
    int end_right_move = position + (8 - position % 8);

    char current_figure = this->get_figure_type();

    // step 1. Straight up
    for(destination_to = position + 8; destination_to < 64; destination_to += 8)
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }

            break;
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_target(target);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            regulars.push_back(new_move);
        }
    }

    // step 2. Straight down
    for(destination_to = position - 8; destination_to >= 0; destination_to -= 8)
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }

            break;
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_target(target);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            regulars.push_back(new_move);
        }
    }

    // step 3. Straight right
    for(destination_to = position + 1; destination_to < end_right_move; destination_to++)
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }

            break;
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_target(target);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            regulars.push_back(new_move);
        }
    }

    // step 4. Straight left
    for(destination_to = position - 1; destination_to >= end_left_move; destination_to--)
    {
        if((target = board.get_figure(destination_to)->get_figure_type()) != EMPTY)
        {
            if(IS_BLACK(current_figure) != IS_BLACK(target))
            {
                new_move = new Move();
                new_move->set_figure(current_figure);
                new_move->set_target(target);
                new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
                regulars.push_back(new_move);
            }

            break;
        }
        else
        {
            new_move = new Move();
            new_move->set_figure(current_figure);
            new_move->set_target(target);
            new_move->set_direction(std::pair<int, int>(destination_from, destination_to));
            regulars.push_back(new_move);
        }
    }

    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves;
    all_moves.first = regulars;
    all_moves.second = nulls;

    return all_moves;
}

Rook::~Rook()
{

}
