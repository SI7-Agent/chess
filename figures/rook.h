#ifndef ROOK_H
#define ROOK_H

#include "../chessboard/chessboard.h"
#include "../base-classes/figure.h"

class Rook : public Figure
{
public:
    Rook();
    Rook(int color);
    ~Rook();

    std::pair<std::vector<Move*>, std::vector<Move*>> get_moves(int position, Chessboard& board);
};

#endif // ROOK_H
