MAKEFILE      = Makefile

CXX           = x86_64-w64-mingw32-g++
CXXFLAGS      = -pipe -O2 -I/usr/local/lib/wx/include/x86_64-w64-mingw32-msw-unicode-static-3.1 -I/usr/local/include/wx-3.1 -D_FILE_OFFSET_BITS=64 -D__WXMSW__  

LINK          = x86_64-w64-mingw32-g++
LFLAGS        = -Wl,-O1
LIBS          = $(SUBLIBS) -L/usr/local/lib -static-libgcc -static-libstdc++ -Wl,--subsystem,windows -mwindows /usr/local/lib/libwx_mswu_xrc-3.1-x86_64-w64-mingw32.a /usr/local/lib/libwx_mswu_qa-3.1-x86_64-w64-mingw32.a /usr/local/lib/libwx_baseu_net-3.1-x86_64-w64-mingw32.a /usr/local/lib/libwx_mswu_html-3.1-x86_64-w64-mingw32.a /usr/local/lib/libwx_mswu_core-3.1-x86_64-w64-mingw32.a /usr/local/lib/libwx_baseu_xml-3.1-x86_64-w64-mingw32.a /usr/local/lib/libwx_baseu-3.1-x86_64-w64-mingw32.a -lwxtiff-3.1-x86_64-w64-mingw32 -lwxjpeg-3.1-x86_64-w64-mingw32 -lwxpng-3.1-x86_64-w64-mingw32 -lwxregexu-3.1-x86_64-w64-mingw32 -lwxscintilla-3.1-x86_64-w64-mingw32 -lwxexpat-3.1-x86_64-w64-mingw32 -lwxzlib-3.1-x86_64-w64-mingw32 -lrpcrt4 -loleaut32 -lole32 -luuid -luxtheme -lwinspool -lwinmm -lshell32 -lshlwapi -lcomctl32 -lcomdlg32 -ladvapi32 -lversion -lwsock32 -lgdi32 -loleacc   

SOURCES       = base-ui-classes/frame.cpp \
		base-ui-classes/wxapp.cpp \
		chessboard/movemaking.cpp \
		figures/bishop.cpp \
		figures/empty.cpp \
		figures/knight.cpp \
		figures/pawn.cpp \
		figures/queen.cpp \
		figures/rook.cpp \
		chessboard/chessboardfen.cpp \
		chessboard/getIndices.cpp \
		chessboard/getPieceColorAt.cpp \
		figures/king.cpp \
		main.cpp \
		chessboard/chessboard.cpp \
		players/aiplayer.cpp \
		players/humanplayer.cpp \
		ui/boardframe.cpp \
		ui/icon.cpp \
		ui/menuframe.cpp \
		ui/panel.cpp \
		ui/transformdialog.cpp 
OBJECTS       = frame.o \
		wxapp.o \
		movemaking.o \
		bishop.o \
		empty.o \
		knight.o \
		pawn.o \
		queen.o \
		rook.o \
		chessboardfen.o \
		getIndices.o \
		getPieceColorAt.o \
		king.o \
		main.o \
		chessboard.o \
		aiplayer.o \
		humanplayer.o \
		boardframe.o \
		icon.o \
		menuframe.o \
		panel.o \
		transformdialog.o
TARGET        = chess

first: all

chess:  $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

all: Makefile chess

frame.o: base-ui-classes/frame.cpp base-ui-classes/frame.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o frame.o base-ui-classes/frame.cpp

wxapp.o: base-ui-classes/wxapp.cpp base-ui-classes/wxapp.h \
		ui/boardframe.h \
		base-ui-classes/frame.h \
		ui/menuframe.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o wxapp.o base-ui-classes/wxapp.cpp

movemaking.o: chessboard/movemaking.cpp chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h \
		figures/bishop.h \
		figures/empty.h \
		figures/knight.h \
		figures/pawn.h \
		figures/rook.h \
		figures/king.h \
		figures/queen.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o movemaking.o chessboard/movemaking.cpp

bishop.o: figures/bishop.cpp figures/bishop.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o bishop.o figures/bishop.cpp

empty.o: figures/empty.cpp figures/empty.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o empty.o figures/empty.cpp

knight.o: figures/knight.cpp figures/knight.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o knight.o figures/knight.cpp

pawn.o: figures/pawn.cpp figures/pawn.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o pawn.o figures/pawn.cpp

queen.o: figures/queen.cpp figures/queen.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h \
		figures/rook.h \
		figures/bishop.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o queen.o figures/queen.cpp

rook.o: figures/rook.cpp figures/rook.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o rook.o figures/rook.cpp

chessboardfen.o: chessboard/chessboardfen.cpp chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o chessboardfen.o chessboard/chessboardfen.cpp

getIndices.o: chessboard/getIndices.cpp chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o getIndices.o chessboard/getIndices.cpp

getPieceColorAt.o: chessboard/getPieceColorAt.cpp chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o getPieceColorAt.o chessboard/getPieceColorAt.cpp

king.o: figures/king.cpp figures/king.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o king.o figures/king.cpp

main.o: main.cpp base-ui-classes/wxapp.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o main.o main.cpp

chessboard.o: chessboard/chessboard.cpp chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h \
		figures/bishop.h \
		figures/empty.h \
		figures/knight.h \
		figures/pawn.h \
		figures/rook.h \
		figures/king.h \
		figures/queen.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o chessboard.o chessboard/chessboard.cpp

aiplayer.o: players/aiplayer.cpp players/aiplayer.h \
		base-classes/player.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		figures/king.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o aiplayer.o players/aiplayer.cpp

humanplayer.o: players/humanplayer.cpp players/humanplayer.h \
		base-classes/player.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o humanplayer.o players/humanplayer.cpp

boardframe.o: ui/boardframe.cpp ui/boardframe.h \
		base-ui-classes/frame.h \
		ui/menuframe.h \
		ui/panel.h \
		ui/icon.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h \
		players/humanplayer.h \
		players/aiplayer.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o boardframe.o ui/boardframe.cpp

icon.o: ui/icon.cpp ui/icon.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o icon.o ui/icon.cpp

menuframe.o: ui/menuframe.cpp ui/menuframe.h \
		base-ui-classes/frame.h \
		ui/boardframe.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o menuframe.o ui/menuframe.cpp

panel.o: ui/panel.cpp ui/panel.h \
		ui/icon.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h \
		ui/boardframe.h \
		base-ui-classes/frame.h \
		ui/transformdialog.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o panel.o ui/panel.cpp

transformdialog.o: ui/transformdialog.cpp ui/transformdialog.h \
		ui/panel.h \
		ui/icon.h \
		chessboard/chessboard.h \
		base-classes/figure.h \
		base-classes/move.h \
		base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o transformdialog.o ui/transformdialog.cpp

