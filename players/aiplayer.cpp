#include "aiplayer.h"
#include "../figures/king.h"

AIPlayer::AIPlayer(int color):
    Player(color)
{
    srand(time(NULL));
}

AIPlayer::~AIPlayer()
{

}

bool AIPlayer::make_move(Chessboard& board, Move* &move)
{
    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves = board.get_all_moves(board.getActiveColor());
    std::vector<Move*> candidates;

    bool quiescent = false;
    int best, tmp;

    best = -KING_VALUE;

    for(std::vector<Move*>::iterator it = all_moves.second.begin(); it != all_moves.second.end(); ++it)
    {
        board.apply_move(*it);
    }

    for(std::vector<Move*>::iterator it = all_moves.first.begin(); it != all_moves.first.end(); ++it)
    {
        board.apply_move(*it);

        if(!((King*)board.get_figure(board.getKingPosition(board.getActiveColor())))->is_vulnerable(board.getKingPosition(board.getActiveColor()), board))
        {

            if((*it)->get_target() != EMPTY)
            {
                quiescent = true;
            }

            tmp = -eval_alpha_beta(board, TOGGLE_COLOR(this->m_color), this->m_search_depth - 1, -WIN_VALUE, -best, quiescent);
            if(tmp > best)
            {
                best = tmp;
                candidates.clear();
                candidates.push_back(*it);
            }
            else if(tmp == best)
            {
                candidates.push_back(*it);
            }
        }

        board.undo_move(*it);
    }

    for(std::vector<Move*>::iterator it = all_moves.second.begin(); it != all_moves.second.end(); ++it)
    {
        board.undo_move(*it);
    }

    if(best < -WIN_VALUE)
    {
        return false;
    }
    else
    {
        move = candidates[rand() % candidates.size()];
        return true;
    }
}

int AIPlayer::eval_alpha_beta(Chessboard &board, int color, int search_depth, int alpha, int beta, bool quiescent)
{
    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves = board.get_all_moves(color);
    int best, tmp;

    if(search_depth <= 0 && !quiescent)
    {
        if(color)
            return -evaluate_board(board);
        else
            return +evaluate_board(board);
    }

    best = -WIN_VALUE;

    for(std::vector<Move*>::iterator it = all_moves.second.begin(); it != all_moves.second.end(); ++it)
        board.apply_move(*it);

    for(std::vector<Move*>::iterator it = all_moves.first.begin(); alpha <= beta && it != all_moves.first.end(); ++it)
    {
        board.apply_move(*it);

        if(!((King*)board.get_figure(board.getKingPosition(color)))->is_vulnerable(board.getKingPosition(color), board))
        {
            if((*it)->get_target() == EMPTY)
                quiescent = false;
            else
                quiescent = true;

            tmp = -eval_alpha_beta(board, TOGGLE_COLOR(color), search_depth - 1, -beta, -alpha, quiescent);
            if(tmp > best)
            {
                best = tmp;
                if(tmp > alpha)
                {
                    alpha = tmp;
                }
            }
        }

        board.undo_move(*it);
    }

    for(std::vector<Move*>::iterator it = all_moves.second.begin(); it != all_moves.second.end(); ++it)
        board.undo_move(*it);

    return best;
}

int AIPlayer::evaluate_board(Chessboard &board)
{
    int figure, sum = 0, summand;

    for(int position = 0; position < 64; position++)
    {
        figure = board.get_figure(position)->get_figure_type();
        switch(FIGURE(figure))
        {
            case PAWN:
                summand = PAWN_VALUE;
                break;
            case ROOK:
                summand = ROOK_VALUE;
                break;
            case KNIGHT:
                summand = KNIGHT_VALUE;
                break;
            case BISHOP:
                summand = BISHOP_VALUE;
                break;
            case QUEEN:
                summand = QUEEN_VALUE;
                break;
            case KING:
                summand = KING_VALUE;
                break;
            default:
                summand = 0;
                break;
        }

        sum += IS_BLACK(figure) ? -summand : summand;
    }

    return sum;
}
