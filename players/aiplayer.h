#ifndef AIPLAYER_H
#define AIPLAYER_H

#define WIN_VALUE  50000	// win the game
#define PAWN_VALUE    300	// 8x
#define ROOK_VALUE    900	// 2x
#define KNIGHT_VALUE  850	// 2x
#define BISHOP_VALUE  840	// 2x
#define QUEEN_VALUE  3000	// 1x
#define KING_VALUE 	 ((PAWN_VALUE * 8) + (ROOK_VALUE * 2) \
                        + (KNIGHT_VALUE * 2) + (BISHOP_VALUE * 2) + QUEEN_VALUE + WIN_VALUE)

#include "../base-classes/player.h"

class AIPlayer: public Player
{
public:
    AIPlayer(int color);
    ~AIPlayer();

    bool make_move(Chessboard& board, Move* &move);

    int eval_alpha_beta(Chessboard &board, int color, int depth, int alpha, int beta, bool quiescent);
    int evaluate_board(Chessboard &board);

protected:
    int m_search_depth = 2;
};

#endif // AIPLAYER_H
