#include "humanplayer.h"

HumanPlayer::HumanPlayer(int color):
    Player(color)
{

}

HumanPlayer::~HumanPlayer()
{

}

bool HumanPlayer::make_move(Chessboard& board, Move* &move)
{
    std::pair<std::vector<Move*>, std::vector<Move*>> all_moves;
    std::vector<Move*> regulars, nulls;
    bool res = true;

    if(!board.is_valid_move(move))
        res = false;

    return res;
}
