#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H

#include "../base-classes/player.h"

class HumanPlayer: public Player
{
public:
    HumanPlayer(int color);
    ~HumanPlayer();

    bool make_move(Chessboard& board, Move* &move);
};

#endif // HUMANPLAYER_H
