#include <iostream>
#include "test_moves.h"
#include "test_takes.h"

int main()
{
    int passed = 0;

    std::vector<std::string> test_movement_descr = {
                                        std::string(" GO MAIN DIAGONAL "),
                                        std::string(" GO SUB DIAGONAL "),
                                        std::string(" GO LEFT "),
                                        std::string(" GO RIGHT "),
                                        std::string(" GO UP "),
                                        std::string(" GO DOWN "),
                                        std::string(" GO THROUGH ANOTHER FIGURE SAME COLOR "),
                                        std::string(" GO THROUGH ANOTHER FIGURE ENEMY COLOR "),
                                        std::string(" TRANSFORM TO QUEEN AT THE END "),
                                   };
    std::vector<bool> test_movement_values = {true, true, false, false, false, false, false, false};
    std::vector<bool(*)()> test_movement_funcs = {
                                        test_main_diagonal_step,
                                        test_sub_diagonal_step,
                                        test_left_step,
                                        test_right_step,
                                        test_up_step,
                                        test_down_step,
                                        test_go_through_white,
                                        test_go_through_black,
                                        test_promotion
                                   };

    std::vector<std::string> test_take_descr = {
                                        std::string(" NORMAL TAKE FIGURE "),
                                        std::string(" TAKE FIGURE SAME COLOR ")
                                   };
    std::vector<bool> test_take_values{true, false};
    std::vector<bool(*)()> test_take_funcs = {
                                        test_normal_take,
                                        test_take_same_color
                                   };

    for (int i = 0; i < test_movement_funcs.size(); i++)
    {
        try
        {
            if(test_move(test_movement_funcs[i]) != test_movement_values[i])
            {
                throw std::string("FAILED\n");
            }
            else
            {
                std::cout << "TEST BISHOP #" << i+1 << test_movement_descr[i] << "PASSED\n";
                passed++;
            }
        }
        catch (std::string msg)
        {
            std::cout << "TEST BISHOP #" << i+1 << test_movement_descr[i] << msg;
        }
    }

    std::cout << "\n";

    for (int i = 0; i < test_take_funcs.size(); i++)
    {
        try
        {
            if(test_move(test_take_funcs[i]) != test_take_values[i])
            {
                throw std::string("FAILED\n");
            }
            else
            {
                std::cout << "TEST BISHOP #" << i+1 << test_take_descr[i] << "PASSED\n";
                passed++;
            }
        }
        catch (std::string msg)
        {
            std::cout << "TEST BISHOP #" << i+1 << test_take_descr[i] << msg;
        }
    }

    assert((passed == (test_movement_funcs.size() + test_take_funcs.size())));

    return 0;
}
