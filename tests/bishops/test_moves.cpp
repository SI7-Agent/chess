#include "test_moves.h"

Chessboard initialize(int white_bishop, int black_bishop, int white_flags, int black_flags)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[white_bishop] = new Bishop();
    figures[white_bishop]->set_figure_type(white_flags);
    figures[black_bishop] = new Bishop();
    figures[black_bishop]->set_figure_type(black_flags);

    Chessboard b = Chessboard(NULL, NULL);
    b.set_figures(figures);

    return b;
}

bool test_move(bool (*func)())
{
    return (*func)();
}

bool test_main_diagonal_step()
{
    bool res = false;

    int bb = 48;
    int wb = 27;

    Chessboard board = initialize(wb, bb);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb+9));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_sub_diagonal_step()
{
    bool res = false;

    int bb = 48;
    int wb = 27;

    Chessboard board = initialize(wb, bb);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb+7));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_left_step()
{
    bool res = false;

    int bb = 48;
    int wb = 27;

    Chessboard board = initialize(wb, bb);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb-1));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_right_step()
{
    bool res = false;

    int bb = 48;
    int wb = 27;

    Chessboard board = initialize(wb, bb);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb+1));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_up_step()
{
    bool res = false;

    int bb = 48;
    int wb = 27;

    Chessboard board = initialize(wb, bb);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb+8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_down_step()
{
    bool res = false;

    int bb = 48;
    int wb = 27;

    Chessboard board = initialize(wb, bb);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb-8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_white()
{
    bool res = false;

    int bb = 35;
    int wb = 27;

    Chessboard board = initialize(wb, bb, BISHOP, BISHOP);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_black()
{
    bool res = false;

    int bb = 35;
    int wb = 27;

    Chessboard board = initialize(wb, bb, SET_BLACK(BISHOP));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_promotion()
{
    bool res = false;

    int bb = 40;
    int wb = 48;

    Chessboard board = initialize(wb, bb, SET_MOVED(BISHOP));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, wb+8));

    res = board.is_valid_move(test);
    board.apply_move(test);

    (FIGURE(board.get_figure(wb+8)->get_figure_type()) == QUEEN) ? res = true : res = false;

    free(test);
    return res;
}
