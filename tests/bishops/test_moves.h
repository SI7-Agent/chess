#ifndef TEST_MOVES_H
#define TEST_MOVES_H

#include "../../chessboard/chessboard.h"
#include "../../figures/empty.h"
#include "../../figures/bishop.h"

Chessboard initialize(int white_bishop, int black_bishop, int white_flags = BISHOP, int black_flags = SET_BLACK(BISHOP));
bool test_move(bool (*func)());

bool test_main_diagonal_step();
bool test_sub_diagonal_step();
bool test_left_step();
bool test_right_step();
bool test_up_step();
bool test_down_step();
bool test_go_through_white();
bool test_go_through_black();
bool test_promotion();

#endif // TEST_MOVES_H
