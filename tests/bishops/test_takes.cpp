#include "test_moves.h"

bool test_take(bool (*func)())
{
    return (*func)();
}

bool test_normal_take()
{
    bool res = false;

    int bb = 34;
    int wb = 27;

    Chessboard board = initialize(wb, bb);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, bb));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_take_same_color()
{
    bool res = false;

    int bb = 34;
    int wb = 27;

    Chessboard board = initialize(wb, bb, BISHOP, BISHOP);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wb, bb));

    res = board.is_valid_move(test);

    free(test);
    return res;
}
