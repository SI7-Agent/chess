#include <iostream>
#include "test_moves.h"
#include "tests_takes.h"

int main()
{
    int passed = 0;

    std::vector<std::string> test_movement_descr = {
                                        std::string(" GO MAIN DIAGONAL 1 CELL "),
                                        std::string(" GO SUB DIAGONAL 1 CELL "),
                                        std::string(" GO MAIN DIAGONAL 2 CELL "),
                                        std::string(" GO SUB DIAGONAL 2 CELL "),
                                        std::string(" GO LEFT 1 CELL "),
                                        std::string(" GO RIGHT 1 CELL "),
                                        std::string(" GO UP 1 CELL "),
                                        std::string(" GO DOWN 1 CELL "),
                                        std::string(" GO LEFT 2 CELL "),
                                        std::string(" GO RIGHT 2 CELL "),
                                        std::string(" GO UP 2 CELL "),
                                        std::string(" GO DOWN 2 CELL "),
                                        std::string(" GO IN CHECK "),
                                        std::string(" GO LEFT CASTLE "),
                                        std::string(" GO RIGHT CASTLE ")
                                   };
    std::vector<bool> test_movement_values = {true, true, false, false, true, true, true, true, false, false, false, false, false, true, true};
    std::vector<bool(*)()> test_movement_funcs = {
                                        test_main_diagonal_step,
                                        test_sub_diagonal_step,
                                        test_main_diagonal_two_steps,
                                        test_sub_diagonal_two_steps,
                                        test_left_step,
                                        test_right_step,
                                        test_up_step,
                                        test_down_step,
                                        test_left_two_steps,
                                        test_right_two_steps,
                                        test_up_two_steps,
                                        test_down_two_steps,
                                        test_incheck_step,
                                        test_left_castling,
                                        test_right_castling
                                   };


    std::vector<std::string> test_take_descr = {
                                        std::string(" NORMAL TAKE FIGURE "),
                                        std::string(" TAKE FIGURE SAME COLOR ")
                                   };
    std::vector<bool> test_take_values{true, false};
    std::vector<bool(*)()> test_take_funcs = {
                                        test_normal_take,
                                        test_take_same_color
                                   };

    for (int i = 0; i < test_movement_funcs.size(); i++)
    {
        try
        {
            if(test_move(test_movement_funcs[i]) != test_movement_values[i])
            {
                throw std::string("FAILED\n");
            }
            else
            {
                std::cout << "TEST KING #" << i+1 << test_movement_descr[i] << "PASSED\n";
                passed++;
            }
        }
        catch (std::string msg)
        {
            std::cout << "TEST KING #" << i+1 << test_movement_descr[i] << msg;
        }
    }

    std::cout << "\n";

    for (int i = 0; i < test_take_funcs.size(); i++)
    {
        try
        {
            if(test_move(test_take_funcs[i]) != test_take_values[i])
            {
                throw std::string("FAILED\n");
            }
            else
            {
                std::cout << "TEST KING #" << i+1 << test_take_descr[i] << "PASSED\n";
                passed++;
            }
        }
        catch (std::string msg)
        {
            std::cout << "TEST KING #" << i+1 << test_take_descr[i] << msg;
        }
    }

    assert((passed == (test_movement_funcs.size() + test_take_funcs.size())));

    return 0;
}
