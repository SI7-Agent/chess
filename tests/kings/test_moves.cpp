#include "test_moves.h"

Chessboard initialize(int white_flags, int black_flags)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[4] = new King();
    figures[4]->set_figure_type(white_flags);
    figures[0] = new Rook();
    figures[7] = new Rook();
    figures[60] = new King();
    figures[60]->set_figure_type(black_flags);
    figures[56] = new Rook(BLACK);
    figures[63] = new Rook(BLACK);

    Chessboard b = Chessboard(NULL, NULL);
    b.set_figures(figures);

    return b;
}

void set_in_centre(Chessboard& board)
{
    Move* test = new Move();
    for(int i = 0; i < 3; i++)
    {
        test->set_direction(std::pair<int, int>(4+i*8, (4+i*8)+8));
        board.is_valid_move(test);
        board.apply_move(test);
    }

    free(test);
}

void set_check_situation(Chessboard& board)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[4] = new King();
    figures[0] = new Rook();
    figures[7] = new Rook();
    figures[60] = new King(BLACK);
    figures[56] = new Rook(BLACK);
    figures[63] = new Rook(BLACK);
    figures[8] = new Rook(BLACK);

    board.set_figures(figures);
}

bool test_move(bool (*func)())
{
    return (*func)();
}

bool test_main_diagonal_step()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+9));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_sub_diagonal_step()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+7));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_main_diagonal_two_steps()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+18));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_sub_diagonal_two_steps()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+14));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_left_step()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4-1));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_right_step()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+1));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_up_step()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_down_step()
{
    bool res = false;

    Chessboard board = initialize();
    set_in_centre(board);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(28, 28-8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_left_two_steps()
{
    bool res = false;

    Chessboard board = initialize(SET_MOVED(KING));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4-2));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_right_two_steps()
{
    bool res = false;

    Chessboard board = initialize(SET_MOVED(KING));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+2));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_up_two_steps()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_down_two_steps()
{
    bool res = false;

    Chessboard board = initialize();
    set_in_centre(board);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(28, 28-16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_incheck_step()
{
    bool res = false;

    Chessboard board = initialize();
    set_check_situation(board);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_left_castling()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4+2));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_right_castling()
{
    bool res = false;

    Chessboard board = initialize();

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 4-2));

    res = board.is_valid_move(test);

    free(test);
    return res;
}
