#ifndef TEST_MOVES_H
#define TEST_MOVES_H

#include "../../chessboard/chessboard.h"
#include "../../figures/empty.h"
#include "../../figures/king.h"
#include "../../figures/rook.h"

Chessboard initialize(int white_flags = KING, int black_flags = SET_BLACK(KING));
void set_in_centre(Chessboard& board);
void set_check_situation(Chessboard& board);
bool test_move(bool (*func)());

bool test_main_diagonal_step();
bool test_sub_diagonal_step();
bool test_main_diagonal_two_steps();
bool test_sub_diagonal_two_steps();
bool test_left_step();
bool test_right_step();
bool test_up_step();
bool test_down_step();
bool test_left_two_steps();
bool test_right_two_steps();
bool test_up_two_steps();
bool test_down_two_steps();
bool test_incheck_step();
bool test_left_castling();
bool test_right_castling();

#endif // TEST_MOVES_H
