#include "test_moves.h"
#include "tests_takes.h"

void set_rook_target(Chessboard& board, int target_flag)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[4] = new King();
    figures[60] = new King();
    figures[11] = new Rook();
    figures[11]->set_figure_type(target_flag);

    board.set_figures(figures);
}

bool test_take(bool (*func)())
{
    return (*func)();
}

bool test_normal_take()
{
    bool res = false;

    Chessboard board = initialize();
    set_rook_target(board);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 11));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_take_same_color()
{
    bool res = false;

    Chessboard board = initialize();
    set_rook_target(board, ROOK);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(4, 11));

    res = board.is_valid_move(test);

    free(test);
    return res;
}
