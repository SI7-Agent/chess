#ifndef TESTS_TAKES_H
#define TESTS_TAKES_H

#include "../../chessboard/chessboard.h"

void set_rook_target(Chessboard& board, int target_flag=SET_BLACK(ROOK));
bool test_take(bool (*func)());

bool test_normal_take();
bool test_take_same_color();

#endif // TESTS_TAKES_H
