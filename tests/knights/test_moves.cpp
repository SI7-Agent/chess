#include "test_moves.h"

Chessboard initialize(int white_knight, int black_knight, int white_flags, int black_flags)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[white_knight] = new Knight();
    figures[white_knight]->set_figure_type(white_flags);
    figures[black_knight] = new Knight();
    figures[black_knight]->set_figure_type(black_flags);

    Chessboard b = Chessboard(NULL, NULL);
    b.set_figures(figures);

    return b;
}

void create_wall(Chessboard& board, int white_knight, int white_flags)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[white_knight] = new Knight();

    std::vector<int> wall{};
    int row = white_knight / 8;
    int col = white_knight % 8;

    if (white_knight+8 < 64)
        wall.push_back(white_knight+8);

    if (white_knight-8 >= 0)
        wall.push_back(white_knight-8);

    if ((white_knight+9 < 64) && ((white_knight+9)/8 == row+1))
        wall.push_back(white_knight+9);

    if ((white_knight+7 < 64) && ((white_knight+7)/8 == row+1))
        wall.push_back(white_knight+7);

    if ((white_knight+1 < 64) && ((white_knight+1)%8 == col+1))
        wall.push_back(white_knight+1);

    if ((white_knight-1 >= 0) && ((white_knight-1)%8 == col-1))
        wall.push_back(white_knight-1);

    if ((white_knight-9 >= 0) && ((white_knight-9)/8 == row-1))
        wall.push_back(white_knight-9);

    if ((white_knight-7 >= 0) && ((white_knight-7)/8 == row-1))
        wall.push_back(white_knight-7);

    for (int i = 0; i < wall.size(); i++)
    {
        free(figures[wall[i]]);
        figures[wall[i]] = new Pawn();
        figures[wall[i]]->set_figure_type(white_flags);
    }

    board.set_figures(figures);
}

bool test_move(bool (*func)())
{
    return (*func)();
}

bool test_left_up_step()
{
    bool res = false;

    int bk = 48;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk+6));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_right_up_step()
{
    bool res = false;

    int bk = 48;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk+10));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_left_down_step()
{
    bool res = false;

    int bk = 48;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk-10));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_right_down_step()
{
    bool res = false;

    int bk = 48;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk-6));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_up_left_step()
{
    bool res = false;

    int bk = 48;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk+15));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_down_left_step()
{
    bool res = false;

    int bk = 48;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk-17));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_up_right_step()
{
    bool res = false;

    int bk = 48;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk+17));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_down_right_step()
{
    bool res = false;

    int bk = 48;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk-15));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_white()
{
    bool res = false;

    int bk = 35;
    int wk = 27;

    Chessboard board = initialize(wk, bk);
    create_wall(board, wk, PAWN);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk+17));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_black()
{
    bool res = false;

    int bk = 35;
    int wk = 27;

    Chessboard board = initialize(wk, bk);
    create_wall(board, wk, SET_BLACK(PAWN));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk+17));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_promotion()
{
    bool res = false;

    int bk = 40;
    int wk = 48;

    Chessboard board = initialize(wk, bk, SET_MOVED(KNIGHT));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, wk+10));

    res = board.is_valid_move(test);
    board.apply_move(test);

    (FIGURE(board.get_figure(wk+10)->get_figure_type()) == QUEEN) ? res = true : res = false;

    free(test);
    return res;
}
