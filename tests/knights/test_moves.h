#ifndef TEST_MOVES_H
#define TEST_MOVES_H

#include "../../chessboard/chessboard.h"
#include "../../figures/empty.h"
#include "../../figures/knight.h"
#include "../../figures/pawn.h"

Chessboard initialize(int white_knight, int black_knight, int white_flags = KNIGHT, int black_flags = SET_BLACK(KNIGHT));
void create_wall(Chessboard& board, int white_knight, int white_flags);
bool test_move(bool (*func)());

bool test_left_up_step();
bool test_right_up_step();
bool test_left_down_step();
bool test_right_down_step();
bool test_up_left_step();
bool test_down_left_step();
bool test_up_right_step();
bool test_down_right_step();
bool test_go_through_white();
bool test_go_through_black();
bool test_promotion();

#endif // TEST_MOVES_H
