#include "test_moves.h"

bool test_take(bool (*func)())
{
    return (*func)();
}

bool test_normal_take()
{
    bool res = false;

    int bk = 37;
    int wk = 27;

    Chessboard board = initialize(wk, bk);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, bk));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_take_same_color()
{
    bool res = false;

    int bk = 37;
    int wk = 27;

    Chessboard board = initialize(wk, bk, KNIGHT, KNIGHT);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wk, bk));

    res = board.is_valid_move(test);

    free(test);
    return res;
}
