#include <iostream>
#include "test_moves.h"
#include "test_takes.h"

int main()
{
    int passed = 0;

    std::vector<std::string> test_movement_descr = {
                                        std::string(" GO ON 1 CELL FORWARD "),
                                        std::string(" GO ON 1 CELL BACKWARD "),
                                        std::string(" GO THROUGH ANOTHER FIGURE SAME COLOR "),
                                        std::string(" GO THROUGH ANOTHER FIGURE ENEMY COLOR "),
                                        std::string(" GO ON 2 CELL FROM START "),
                                        std::string(" GO ON 2 CELL FROM START ANY TIME "),
                                        std::string(" TRANSFORM TO QUEEN AT THE END "),
                                   };
    std::vector<bool> test_movement_values = {true, false, false, false, true, false, true};
    std::vector<bool(*)()> test_movement_funcs = {
                                        test_one_step,
                                        test_one_step_back,
                                        test_go_through_white,
                                        test_go_through_black,
                                        test_two_steps_first_move,
                                        test_two_steps_move,
                                        test_promotion
                                   };

    std::vector<std::string> test_take_descr = {
                                        std::string(" NORMAL TAKE FIGURE "),
                                        std::string(" TAKE FIGURE SAME COLOR "),
                                        std::string(" TAKE FIGURE BY GO BACK "),
                                        std::string(" TAKE FIGURE BY GO FRONT "),
                                        std::string(" TAKE PAWN EN PASSANTE MOVE ")
                                   };
    std::vector<bool> test_take_values{true, false, false, false, true};
    std::vector<bool(*)()> test_take_funcs = {
                                        test_normal_take,
                                        test_take_same_color,
                                        test_take_back,
                                        test_take_step_front,
                                        test_enpassante_take
                                   };

    for (int i = 0; i < test_movement_funcs.size(); i++)
    {
        try
        {
            if(test_move(test_movement_funcs[i]) != test_movement_values[i])
            {
                throw std::string("FAILED\n");
            }
            else
            {
                std::cout << "TEST PAWN #" << i+1 << test_movement_descr[i] << "PASSED\n";
                passed++;
            }
        }
        catch (std::string msg)
        {
            std::cout << "TEST PAWN #" << i+1 << test_movement_descr[i] << msg;
        }
    }

    std::cout << "\n";

    for (int i = 0; i < test_take_funcs.size(); i++)
    {
        try
        {
            if(test_move(test_take_funcs[i]) != test_take_values[i])
            {
                throw std::string("FAILED\n");
            }
            else
            {
                std::cout << "TEST PAWN #" << i+1 << test_take_descr[i] << "PASSED\n";
                passed++;
            }
        }
        catch (std::string msg)
        {
            std::cout << "TEST PAWN #" << i+1 << test_take_descr[i] << msg;
        }
    }

    assert((passed == (test_movement_funcs.size() + test_take_funcs.size())));

    return 0;
}
