TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    ../../chessboard/movemaking.cpp \
    ../../figures/bishop.cpp \
    ../../figures/empty.cpp \
    ../../figures/knight.cpp \
    ../../figures/pawn.cpp \
    ../../figures/queen.cpp \
    ../../figures/rook.cpp \
    ../../chessboard/chessboardfen.cpp \
    ../../chessboard/getIndices.cpp \
    ../../chessboard/getPieceColorAt.cpp \
    ../../figures/king.cpp \
    main.cpp \
    test_moves.cpp \
    ../../chessboard/chessboard.cpp \
    test_takes.cpp

HEADERS += \
    test_moves.h \
    ../../base-classes/figure.h \
    ../../base-classes/move.h \
    ../../base-classes/player.h \
    ../../chessboard/chessboard.h \
    ../../figures/bishop.h \
    ../../figures/empty.h \
    ../../figures/king.h \
    ../../figures/knight.h \
    ../../figures/pawn.h \
    ../../figures/queen.h \
    ../../figures/rook.h \
    test_takes.h
