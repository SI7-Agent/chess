#include "test_moves.h"

Chessboard initialize(int white_pawn, int black_pawn, int white_flags, int black_flags)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[white_pawn] = new Pawn();
    figures[white_pawn]->set_figure_type(white_flags);
    figures[black_pawn] = new Pawn();
    figures[black_pawn]->set_figure_type(black_flags);

    Chessboard b = Chessboard(NULL, NULL);
    b.set_figures(figures);

    return b;
}

bool test_move(bool (*func)())
{
    return (*func)();
}

bool test_one_step()
{
    bool res = false;

    int bp = 20;
    int wp = 10;

    Chessboard board = initialize(wp, bp);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, wp+8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_one_step_back()
{
    bool res = false;

    int bp = 20;
    int wp = 10;

    Chessboard board = initialize(wp, bp);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, wp-8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_white()
{
    bool res = false;

    int bp = 18;
    int wp = 10;

    Chessboard board = initialize(wp, bp, PAWN, PAWN);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, wp+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_black()
{
    bool res = false;

    int bp = 18;
    int wp = 10;

    Chessboard board = initialize(wp, bp, SET_BLACK(PAWN));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, wp+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_two_steps_first_move()
{
    bool res = false;

    int bp = 20;
    int wp = 10;

    Chessboard board = initialize(wp, bp);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, wp+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_two_steps_move()
{
    bool res = false;

    int bp = 20;
    int wp = 10;

    Chessboard board = initialize(wp, bp, SET_MOVED(PAWN));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, wp+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_promotion()
{
    bool res = false;

    int bp = 20;
    int wp = 48;

    Chessboard board = initialize(wp, bp, SET_MOVED(PAWN));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, wp+8));

    res = board.is_valid_move(test);
    board.apply_move(test);

    (FIGURE(board.get_figure(wp+8)->get_figure_type()) == QUEEN) ? res = true : res = false;

    free(test);
    return res;
}
