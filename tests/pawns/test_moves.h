#ifndef TEST_MOVES_H
#define TEST_MOVES_H

#include "../../chessboard/chessboard.h"
#include "../../figures/empty.h"
#include "../../figures/pawn.h"

Chessboard initialize(int white_pawn, int black_pawn, int white_flags = PAWN, int black_flags = SET_BLACK(PAWN));
bool test_move(bool (*func)());

bool test_one_step();
bool test_one_step_back();
bool test_go_through_white();
bool test_go_through_black();
bool test_two_steps_first_move();
bool test_two_steps_move();
bool test_promotion();

#endif // TEST_MOVES_H
