#include "test_takes.h"

bool test_take(bool (*func)())
{
    return (*func)();
}

bool test_normal_take()
{
    bool res = false;

    int bp = 19;
    int wp = 10;

    Chessboard board = initialize(wp, bp);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, bp));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_take_same_color()
{
    bool res = false;

    int bp = 19;
    int wp = 10;

    Chessboard board = initialize(wp, bp, PAWN, PAWN);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, bp));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_take_back()
{
    bool res = false;

    int bp = 19;
    int wp = 10;

    Chessboard board = initialize(bp, wp);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(bp, wp));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_take_step_front()
{
    bool res = false;

    int bp = 18;
    int wp = 10;

    Chessboard board = initialize(wp, bp);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, bp));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_enpassante_take()
{
    bool res = false;

    int bp = 36;
    int wp = 35;

    Chessboard board = initialize(wp, bp, PAWN, SET_PASSANT(SET_BLACK(PAWN)));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, bp+8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}
