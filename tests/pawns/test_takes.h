#ifndef TEST_TAKES_H
#define TEST_TAKES_H

#include "test_moves.h"

bool test_take(bool (*func)());

bool test_normal_take();
bool test_take_same_color();
bool test_take_back();
bool test_take_step_front();
bool test_enpassante_take();

#endif // TEST_TAKES_H
