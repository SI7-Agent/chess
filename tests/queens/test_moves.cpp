#include "test_moves.h"

Chessboard initialize(int white_queen, int black_queen, int white_flags, int black_flags)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[white_queen] = new Queen();
    figures[white_queen]->set_figure_type(white_flags);
    figures[black_queen] = new Queen();
    figures[black_queen]->set_figure_type(black_flags);

    Chessboard b = Chessboard(NULL, NULL);
    b.set_figures(figures);

    return b;
}

bool test_move(bool (*func)())
{
    return (*func)();
}

bool test_main_diagonal_step()
{
    bool res = false;

    int bq = 48;
    int wq = 27;

    Chessboard board = initialize(wq, bq);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, wq+9));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_sub_diagonal_step()
{
    bool res = false;

    int bq = 48;
    int wq = 27;

    Chessboard board = initialize(wq, bq);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, wq+7));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_left_step()
{
    bool res = false;

    int bq = 48;
    int wq = 27;

    Chessboard board = initialize(wq, bq);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, wq-1));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_right_step()
{
    bool res = false;

    int bq = 48;
    int wq = 27;

    Chessboard board = initialize(wq, bq);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, wq+1));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_up_step()
{
    bool res = false;

    int bq = 48;
    int wq = 27;

    Chessboard board = initialize(wq, bq);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, wq+8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_down_step()
{
    bool res = false;

    int bq = 48;
    int wq = 27;

    Chessboard board = initialize(wq, bq);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, wq-8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_white()
{
    bool res = false;

    int bq = 35;
    int wq = 27;

    Chessboard board = initialize(wq, bq, QUEEN, QUEEN);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, wq+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_black()
{
    bool res = false;

    int bq = 35;
    int wq = 27;

    Chessboard board = initialize(wq, bq, SET_BLACK(QUEEN));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, wq+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}
