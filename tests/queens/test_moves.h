#ifndef TEST_MOVES_H
#define TEST_MOVES_H

#include "../../chessboard/chessboard.h"
#include "../../figures/empty.h"
#include "../../figures/queen.h"

Chessboard initialize(int white_queen, int black_queen, int white_flags = QUEEN, int black_flags = SET_BLACK(QUEEN));
bool test_move(bool (*func)());

bool test_main_diagonal_step();
bool test_sub_diagonal_step();
bool test_left_step();
bool test_right_step();
bool test_up_step();
bool test_down_step();
bool test_go_through_white();
bool test_go_through_black();

#endif // TEST_MOVES_H
