#include "test_moves.h"

bool test_take(bool (*func)())
{
    return (*func)();
}

bool test_normal_take()
{
    bool res = false;

    int bq = 34;
    int wq = 27;

    Chessboard board = initialize(wq, bq);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, bq));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_take_same_color()
{
    bool res = false;

    int bq = 34;
    int wq = 27;

    Chessboard board = initialize(wq, bq, QUEEN, QUEEN);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wq, bq));

    res = board.is_valid_move(test);

    free(test);
    return res;
}
