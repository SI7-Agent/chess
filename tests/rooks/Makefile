MAKEFILE      = Makefile

CXX           = g++
CXXFLAGS      = -pipe -O2 -std=gnu++11 -Wall -Wextra -fPIC $(DEFINES)
INCPATH       = -I. -I/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++

LINK          = g++
LFLAGS        = -Wl,-O1
LIBS          = $(SUBLIBS)    

SOURCES       = ../../chessboard/movemaking.cpp \
		../../figures/bishop.cpp \
		../../figures/empty.cpp \
		../../figures/knight.cpp \
		../../figures/pawn.cpp \
		../../figures/queen.cpp \
		../../figures/rook.cpp \
		../../chessboard/chessboardfen.cpp \
		../../chessboard/getIndices.cpp \
		../../chessboard/getPieceColorAt.cpp \
		../../figures/king.cpp \
		main.cpp \
		test_moves.cpp \
		../../chessboard/chessboard.cpp \
		test_takes.cpp 
OBJECTS       = movemaking.o \
		bishop.o \
		empty.o \
		knight.o \
		pawn.o \
		queen.o \
		rook.o \
		chessboardfen.o \
		getIndices.o \
		getPieceColorAt.o \
		king.o \
		main.o \
		test_moves.o \
		chessboard.o \
		test_takes.o
TARGET        = rooks

first: all

rooks:  $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

all: Makefile rooks

movemaking.o: ../../chessboard/movemaking.cpp ../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h \
		../../figures/bishop.h \
		../../figures/empty.h \
		../../figures/knight.h \
		../../figures/pawn.h \
		../../figures/rook.h \
		../../figures/king.h \
		../../figures/queen.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o movemaking.o ../../chessboard/movemaking.cpp

bishop.o: ../../figures/bishop.cpp ../../figures/bishop.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o bishop.o ../../figures/bishop.cpp

empty.o: ../../figures/empty.cpp ../../figures/empty.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o empty.o ../../figures/empty.cpp

knight.o: ../../figures/knight.cpp ../../figures/knight.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o knight.o ../../figures/knight.cpp

pawn.o: ../../figures/pawn.cpp ../../figures/pawn.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o pawn.o ../../figures/pawn.cpp

queen.o: ../../figures/queen.cpp ../../figures/queen.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h \
		../../figures/rook.h \
		../../figures/bishop.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o queen.o ../../figures/queen.cpp

rook.o: ../../figures/rook.cpp ../../figures/rook.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o rook.o ../../figures/rook.cpp

chessboardfen.o: ../../chessboard/chessboardfen.cpp ../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o chessboardfen.o ../../chessboard/chessboardfen.cpp

getIndices.o: ../../chessboard/getIndices.cpp ../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o getIndices.o ../../chessboard/getIndices.cpp

getPieceColorAt.o: ../../chessboard/getPieceColorAt.cpp ../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o getPieceColorAt.o ../../chessboard/getPieceColorAt.cpp

king.o: ../../figures/king.cpp ../../figures/king.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o king.o ../../figures/king.cpp

main.o: main.cpp test_moves.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h \
		../../figures/empty.h \
		../../figures/rook.h \
		test_takes.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o main.o main.cpp

test_moves.o: test_moves.cpp test_moves.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h \
		../../figures/empty.h \
		../../figures/rook.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o test_moves.o test_moves.cpp

chessboard.o: ../../chessboard/chessboard.cpp ../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h \
		../../figures/bishop.h \
		../../figures/empty.h \
		../../figures/knight.h \
		../../figures/pawn.h \
		../../figures/rook.h \
		../../figures/king.h \
		../../figures/queen.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o chessboard.o ../../chessboard/chessboard.cpp

test_takes.o: test_takes.cpp test_moves.h \
		../../chessboard/chessboard.h \
		../../base-classes/figure.h \
		../../base-classes/move.h \
		../../base-classes/player.h \
		../../figures/empty.h \
		../../figures/rook.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o test_takes.o test_takes.cpp

