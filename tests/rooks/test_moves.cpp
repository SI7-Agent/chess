#include "test_moves.h"

Chessboard initialize(int white_rook, int black_rook, int white_flags, int black_flags)
{
    std::vector<Figure*>figures(64);
    for (int i = 0; i < 64; i++)
        figures[i] = new Empty();

    figures[white_rook] = new Rook();
    figures[white_rook]->set_figure_type(white_flags);
    figures[black_rook] = new Rook();
    figures[black_rook]->set_figure_type(black_flags);

    Chessboard b = Chessboard(NULL, NULL);
    b.set_figures(figures);

    return b;
}

bool test_move(bool (*func)())
{
    return (*func)();
}

bool test_left_step()
{
    bool res = false;

    int br = 48;
    int wr = 27;

    Chessboard board = initialize(wr, br);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, wr-1));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_right_step()
{
    bool res = false;

    int br = 48;
    int wr = 27;

    Chessboard board = initialize(wr, br);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, wr+1));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_up_step()
{
    bool res = false;

    int br = 48;
    int wr = 27;

    Chessboard board = initialize(wr, br);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, wr+8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_down_step()
{
    bool res = false;

    int br = 48;
    int wr = 27;

    Chessboard board = initialize(wr, br);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, wr-8));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_white()
{
    bool res = false;

    int br = 35;
    int wr = 27;

    Chessboard board = initialize(wr, br, ROOK, ROOK);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, wr+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_go_through_black()
{
    bool res = false;

    int br = 35;
    int wr = 27;

    Chessboard board = initialize(wr, br, SET_BLACK(ROOK));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, wr+16));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_main_diagonal_step()
{
    bool res = false;

    int br = 48;
    int wr = 27;

    Chessboard board = initialize(wr, br);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, wr+9));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_sub_diagonal_step()
{
    bool res = false;

    int br = 48;
    int wr = 27;

    Chessboard board = initialize(wr, br);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, wr+7));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_promotion()
{
    bool res = false;

    int bp = 40;
    int wp = 48;

    Chessboard board = initialize(wp, bp, SET_MOVED(ROOK));

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wp, wp+8));

    res = board.is_valid_move(test);
    board.apply_move(test);

    (FIGURE(board.get_figure(wp+8)->get_figure_type()) == QUEEN) ? res = true : res = false;

    free(test);
    return res;
}
