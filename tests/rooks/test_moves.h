#ifndef TEST_MOVES_H
#define TEST_MOVES_H

#include "../../chessboard/chessboard.h"
#include "../../figures/empty.h"
#include "../../figures/rook.h"

Chessboard initialize(int white_rook, int black_rook, int white_flags = ROOK, int black_flags = SET_BLACK(ROOK));
bool test_move(bool (*func)());

bool test_left_step();
bool test_right_step();
bool test_up_step();
bool test_down_step();
bool test_go_through_white();
bool test_go_through_black();
bool test_main_diagonal_step();
bool test_sub_diagonal_step();
bool test_promotion();

#endif // TEST_MOVES_H
