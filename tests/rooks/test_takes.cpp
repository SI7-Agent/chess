#include "test_moves.h"

bool test_take(bool (*func)())
{
    return (*func)();
}

bool test_normal_take()
{
    bool res = false;

    int br = 35;
    int wr = 27;

    Chessboard board = initialize(wr, br);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, br));

    res = board.is_valid_move(test);

    free(test);
    return res;
}

bool test_take_same_color()
{
    bool res = false;

    int br = 35;
    int wr = 27;

    Chessboard board = initialize(wr, br, ROOK, ROOK);

    Move* test = new Move();
    test->set_direction(std::pair<int, int>(wr, br));

    res = board.is_valid_move(test);

    free(test);
    return res;
}
