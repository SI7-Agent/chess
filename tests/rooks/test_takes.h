#ifndef TEST_TAKES_H
#define TEST_TAKES_H

bool test_take(bool (*func)());

bool test_normal_take();
bool test_take_same_color();

#endif // TEST_TAKES_H
