#include <iostream>
#include "test_statuses.h"

int main()
{
    int passed = 0;

    std::vector<std::string> test_status_descr = {
                                        std::string(" CHECK "),
                                        std::string(" CHECKMATE "),
                                        std::string(" STALEMATE ")
                                   };
    std::vector<bool> test_status_values{true, true, true};
    std::vector<bool(*)()> test_status_funcs = {
                                        test_check,
                                        test_checkmate,
                                        test_stalemate
                                   };

    for (int i = 0; i < test_status_funcs.size(); i++)
    {
        try
        {
            if(test_move(test_status_funcs[i]) != test_status_values[i])
            {
                throw std::string("FAILED\n");
            }
            else
            {
                std::cout << "TEST STATUS #" << i+1 << test_status_descr[i] << "PASSED\n";
                passed++;
            }
        }
        catch (std::string msg)
        {
            std::cout << "TEST STATUS #" << i+1 << test_status_descr[i] << msg;
        }
    }

    assert((passed == test_status_funcs.size()));

    return 0;
}
