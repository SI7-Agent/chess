#include "test_statuses.h"

bool test_move(bool (*func)())
{
    return (*func)();
}

bool test_check()
{
    bool res = false;

    Chessboard board = Chessboard(NULL, NULL);

    std::vector<std::pair<int, int>> moves = {
        std::pair<int, int>(12, 20),
        std::pair<int, int>(53, 45),
        std::pair<int, int>(3, 39)
    };

    Move* turn = new Move();
    for (int i = 0; i < moves.size(); i++)
    {
        turn->set_direction(moves[i]);
        board.is_valid_move(turn);
        board.apply_move(turn);
        board.changeActiveColor();
    }

    int status = board.get_status();
    if (status == Player::InCheck)
        res = true;

    return res;
}

bool test_checkmate()
{
    bool res = false;

    Chessboard board = Chessboard(NULL, NULL);

    std::vector<std::pair<int, int>> moves = {
        std::pair<int, int>(12, 28),
        std::pair<int, int>(52, 36),
        std::pair<int, int>(3, 39),
        std::pair<int, int>(60, 52),
        std::pair<int, int>(39, 36)
    };

    Move* turn = new Move();
    for (int i = 0; i < moves.size(); i++)
    {
        turn->set_direction(moves[i]);
        board.is_valid_move(turn);
        board.apply_move(turn);
        board.changeActiveColor();
    }

    int status = board.get_status();
    if (status == Player::Checkmate)
        res = true;

    return res;
}

bool test_stalemate()
{
    bool res = false;

    Chessboard board = Chessboard(NULL, NULL);

    std::vector<std::pair<int, int>> moves = {
        std::pair<int, int>(12, 20), std::pair<int, int>(48, 32),
        std::pair<int, int>(3, 39), std::pair<int, int>(56, 40),
        std::pair<int, int>(39, 32), std::pair<int, int>(55, 39),
        std::pair<int, int>(15, 31), std::pair<int, int>(40, 47),
        std::pair<int, int>(32, 50), std::pair<int, int>(53, 45),
        std::pair<int, int>(50, 51), std::pair<int, int>(60, 53),
        std::pair<int, int>(51, 49), std::pair<int, int>(59, 19),
        std::pair<int, int>(49, 57), std::pair<int, int>(19, 55),
        std::pair<int, int>(57, 58), std::pair<int, int>(53, 46),
        std::pair<int, int>(58, 44)
    };

    Move* turn = new Move();
    for (int i = 0; i < moves.size(); i++)
    {
        turn->set_direction(moves[i]);
        board.is_valid_move(turn);
        board.apply_move(turn);
        board.changeActiveColor();
    }

    int status = board.get_status();
    if (status == Player::Stalemate)
        res = true;

    return res;
}
