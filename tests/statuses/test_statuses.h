#ifndef TEST_STATUSES_H
#define TEST_STATUSES_H

#include "../../chessboard/chessboard.h"

bool test_move(bool (*func)());

bool test_check();
bool test_checkmate();
bool test_stalemate();

#endif // TEST_STATUSES_H
