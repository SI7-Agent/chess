#include "boardframe.h"
#include "menuframe.h"
#include "panel.h"
#include "../chessboard/chessboard.h"
#include "../players/humanplayer.h"
#include "../players/aiplayer.h"

BoardFrame::BoardFrame(const wxString& title, Frame* parent):
    Frame(title)
{
    m_parent = parent;
    Player* p1 = new HumanPlayer(WHITE);
    Player* p2;
    int f = ((MenuFrame*)parent)->get_mode();
    f ? p2 = new HumanPlayer(BLACK) : p2 = new AIPlayer(BLACK);

    Chessboard board = Chessboard(p1, p2);
    Panel* panel = new Panel(this, board, f);
    panel->SetFocus();

    Bind(wxEVT_CLOSE_WINDOW, &BoardFrame::drop_window, this);

    Centre();
}

void BoardFrame::drop_window(wxCloseEvent & WXUNUSED(event))
{
    m_parent->Show();
    Destroy();
}
