#ifndef BOARDFRAME_H
#define BOARDFRAME_H

#include "../base-ui-classes/frame.h"

class BoardFrame : public Frame
{
public:
    BoardFrame(const wxString& title, Frame* parent);

private:
    void drop_window(wxCloseEvent & WXUNUSED(event));
};

#endif // BOARDFRAME_H
