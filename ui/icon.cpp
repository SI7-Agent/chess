#include "icon.h"

Icon::Icon(const wxString path)
{
    m_captured = false;
    m_dragging = false;
    m_img = wxImage(path, wxBITMAP_TYPE_ANY);
}

void Icon::draw(wxDC& dc, int length)
{
    m_spacingLength = length;
    m_iconSize = length * 0.8;
    m_spacingOffset = length * 0.1;
    if(m_dragging){
        dc.DrawBitmap(
            wxBitmap(m_img.Scale(m_iconSize, m_iconSize)),
            m_pixelX, m_pixelY, false
        );
    } else if(!m_captured){
        dc.DrawBitmap(
            wxBitmap(m_img.Scale(m_iconSize, m_iconSize)),
            m_boardX * length + m_spacingOffset, m_boardY * length + m_spacingOffset, false
        );
    }
}

bool Icon::begin_move(wxPoint pt)
{
    if(m_captured){
        return false;
    }
    m_pixelX = m_boardX * m_spacingLength + m_spacingOffset;
    m_pixelY = m_boardY * m_spacingLength + m_spacingOffset;
    if(m_pixelX <= pt.x && pt.x <= m_pixelX + m_iconSize && m_pixelY <= pt.y && pt.y <= m_pixelY + m_iconSize){
        m_pixelX = pt.x - m_iconSize / 2;
        m_pixelY = pt.y - m_iconSize / 2;
        m_dragging = true;
        return true;
    } else {
        return false;
    }
}

void Icon::finish_move(wxPoint pt, bool moved)
{
    if(m_dragging){
        if(moved){
            m_boardX = pt.x / m_spacingLength;
            m_boardY = pt.y / m_spacingLength;
        }
        m_dragging = false;
    }
}

void Icon::move(wxPoint pt)
{
    if(m_dragging){
        m_pixelX = pt.x - m_iconSize / 2;
        m_pixelY = pt.y - m_iconSize / 2;
    }
}
