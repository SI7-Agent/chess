#ifndef ICON_H
#define ICON_H

#include <wx/wx.h>

class Icon
{
public:
    Icon(const wxString path);
    void draw(wxDC& dc, int length);
    bool begin_move(wxPoint pt);
    void finish_move(wxPoint pt, bool moved);
    void move(wxPoint pt);

    void place_at(int x, int y)
    {
        m_boardX = x;
        m_boardY = y;
    }

    bool is_dragging()
    {
        return m_dragging;
    }

    int get_board_x()
    {
        return m_boardX;
    }

    int get_board_y()
    {
        return m_boardY;
    }

    void remove()
    {
        m_captured = true;
    }

private:
    int m_spacingLength;
    int m_iconSize;
    int m_spacingOffset;
    bool m_dragging;
    bool m_captured;
    int m_boardX, m_boardY;
    int m_pixelX, m_pixelY;
    wxImage m_img;
};

#endif // ICON_H
