#include "menuframe.h"
#include "boardframe.h"

MenuFrame::MenuFrame(const wxString& title):
    Frame(title)
{
    int width = this->GetClientSize().GetX();
    int height = this->GetClientSize().GetY();
    int button_num = 3;

    int padding_x = 10;
    int padding_y = 45;

    int place_area_x = width - 2 * padding_x;
    int place_area_y = height - 2 * padding_y;

    int b_width = place_area_x - 2 * padding_x;
    int b_height = place_area_y/button_num - 2 * padding_y;

    SetMaxSize(wxSize(width, height));
    SetMinSize(wxSize(width, height));

    wxButton *button_s = new wxButton(this, wx_Button_singleplayer, wxT("Singleplayer"), wxPoint(place_area_x-b_width, padding_y), wxSize(b_width, b_height));
    Connect(wx_Button_singleplayer, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MenuFrame::singleplayer));

    wxButton *button_lm = new wxButton(this, wx_Button_localmultiplayer, wxT("Local Multiplayer"), wxPoint(place_area_x-b_width, padding_y + place_area_y/button_num), wxSize(b_width, b_height));
    Connect(wx_Button_localmultiplayer, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MenuFrame::local_multiplayer));

    wxButton *button_q = new wxButton(this, wx_Button_quit, wxT("Quit"), wxPoint(place_area_x-b_width, padding_y + 2 * place_area_y/button_num), wxSize(b_width, b_height));
    Connect(wx_Button_quit, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MenuFrame::quit));

    Centre();
}

void MenuFrame::singleplayer(wxCommandEvent & WXUNUSED(event))
{
    m_mode = 0;
    m_board = new BoardFrame(wxT("ImmortalChess"), this);

    this->Hide();
    m_board->Show();
}

void MenuFrame::local_multiplayer(wxCommandEvent & WXUNUSED(event))
{
    m_mode = 1;
    m_board = new BoardFrame(wxT("ImmortalChess"), this);

    this->Hide();
    m_board->Show();
}

void MenuFrame::quit(wxCommandEvent & WXUNUSED(event))
{
    Destroy();
}

int MenuFrame::get_mode()
{
    return m_mode;
}
