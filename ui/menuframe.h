#ifndef MENUFRAME_H
#define MENUFRAME_H

#include "../base-ui-classes/frame.h"

class MenuFrame: public Frame
{
public:
    MenuFrame(const wxString& title);
    void singleplayer(wxCommandEvent & WXUNUSED(event));
    void local_multiplayer(wxCommandEvent & WXUNUSED(event));
    void quit(wxCommandEvent & WXUNUSED(event));

    int get_mode();

private:
    Frame* m_board;
    int m_counter = 0;
    int m_mode;

    enum m_ids
    {
        wx_Button_singleplayer = 0,
        wx_Button_localmultiplayer,
        wx_Button_netmultiplayer,
        wx_Button_quit
    };
};

#endif // MENUFRAME_H
