#include "panel.h"
#include "../base-classes/move.h"
#include "boardframe.h"
#include "transformdialog.h"

Panel::Panel(wxFrame *parent, Chessboard& board, int f):
    wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE),
    m_board(board)
{
    m_icon_path['p'] = wxT("ui/img/black_pawn.png");
    m_icon_path['n'] = wxT("ui/img/black_knight.png");
    m_icon_path['b'] = wxT("ui/img/black_bishop.png");
    m_icon_path['r'] = wxT("ui/img/black_rook.png");
    m_icon_path['q'] = wxT("ui/img/black_queen.png");
    m_icon_path['k'] = wxT("ui/img/black_king.png");
    m_icon_path['P'] = wxT("ui/img/white_pawn.png");
    m_icon_path['N'] = wxT("ui/img/white_knight.png");
    m_icon_path['B'] = wxT("ui/img/white_bishop.png");
    m_icon_path['R'] = wxT("ui/img/white_rook.png");
    m_icon_path['Q'] = wxT("ui/img/white_queen.png");
    m_icon_path['K'] = wxT("ui/img/white_king.png");

    m_parent = parent;
    m_boardLength = board.m_length;
    m_mode = f;
    load_figures();

    Connect(wxEVT_PAINT, wxPaintEventHandler(Panel::on_paint));
    Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(Panel::on_mouse_down));
    Connect(wxEVT_LEFT_UP, wxMouseEventHandler(Panel::on_mouse_up));
    Connect(wxEVT_MOTION, wxMouseEventHandler(Panel::on_move));
}

void Panel::load_figures()
{ 
    m_icon.resize(0);
    char piece;

    for(int i = 0; i < m_boardLength; i++)
    {
        for(int j = 0; j < m_boardLength; j++)
        {
            piece = m_board.abbr(i * m_boardLength + j);
            if(piece != '-')
            {
                Icon* icon = new Icon(m_icon_path[piece]);
                icon->place_at(j, m_boardLength - 1 - i);
                m_icon.push_back(icon);
            }
        }
    }
}

void Panel::on_paint(wxPaintEvent& event)
{
    wxPaintDC dc(this);

    for (int i = 0; i < m_boardLength; i++)
    {
        for (int j = 0; j < m_boardLength; j++)
        {
            draw_square(dc, i, j);
        }
    }

    for(int i = 0; i < m_icon.size(); i++)
    {
        m_icon[i]->draw(dc, square_length());
    }
}

void Panel::on_mouse_down(wxMouseEvent& event)
{
    for(int i = 0; i < m_icon.size(); i++)
    {
        if(m_icon[i]->begin_move(event.GetPosition()))
        {
            int y = m_icon[i]->get_board_y();
            int x = m_icon[i]->get_board_x();
            m_from = (7-y)*8 + x;

            m_origin = m_board.getNotation(y, x);
            break;
        }
    }
}

void Panel::play_with_human()
{
    bool success = false;
    class Move* test = new class Move();
    test->set_direction(std::pair<int, int>(m_from, m_to));

    if (m_board.getActiveColor() == WHITE)
    {
        bool val = m_board.m_player1->make_move(m_board, test);
        if (val)
        {
            success = true;
        }
    }
    else
    {
        bool val = m_board.m_player2->make_move(m_board, test);
        if (val)
        {
            success = true;
        }
    }

    if (success == true)
    {
        std::vector<class Move*> nulls = m_board.get_all_moves(m_board.getActiveColor()).second;
        for(std::vector<class Move*>::iterator it = nulls.begin(); it != nulls.end(); ++it)
            m_board.apply_move(*it);

        if(((test->get_to()/8 == 7 ) || (test->get_to()/8 == 0)) && (FIGURE(test->get_figure()) == PAWN))
        {
            TransformDialog* pawn_transform = new TransformDialog(wxT("PawnTransformation"), this);
            pawn_transform->ShowModal();
        }

        m_board.apply_move(test);
        m_board.changeActiveColor();
    }

    load_figures();
    Refresh(true);
    free(test);

    check_status();
}

void Panel::play_with_ai()
{
    bool success = false;
    bool success_ai = false;
    class Move* test_ai = new class Move();
    class Move* test_human = new class Move();
    test_human->set_direction(std::pair<int, int>(m_from, m_to));

    if (m_board.getActiveColor() == WHITE)
    {
        bool val = m_board.m_player1->make_move(m_board, test_human);
        if (val)
        {
            success = true;
        }
    }

    if (success == true)
    {
        std::vector<class Move*> nulls = m_board.get_all_moves(m_board.getActiveColor()).second;
        for(std::vector<class Move*>::iterator it = nulls.begin(); it != nulls.end(); ++it)
            m_board.apply_move(*it);

        if((test_human->get_to()/8 == 7 ) && (FIGURE(test_human->get_figure()) == PAWN))
        {
            TransformDialog* pawn_transform = new TransformDialog(wxT("PawnTransformation"), this);
            pawn_transform->ShowModal();
        }

        m_board.apply_move(test_human);
        m_board.changeActiveColor();
    }

    load_figures();
    Refresh(true);
    check_status();

    if ((m_board.getActiveColor() == BLACK) and (success == true))
    {
        bool val = m_board.m_player2->make_move(m_board, test_ai);
        if (val)
        {
            success_ai = true;
        }
    }

    if (success_ai == true)
    {
        std::vector<class Move*> nulls = m_board.get_all_moves(m_board.getActiveColor()).second;
        for(std::vector<class Move*>::iterator it = nulls.begin(); it != nulls.end(); ++it)
            m_board.apply_move(*it);

        m_board.set_pawn_transform(0);
        m_board.apply_move(test_ai);
        m_board.changeActiveColor();
    }

    load_figures();
    Refresh(true);
    check_status();

    free(test_ai);
    free(test_human);
}

void Panel::on_mouse_up(wxMouseEvent& event)
{
    for(int i = 0; i < m_icon.size(); i++)
    {
        if (m_icon[i]->is_dragging())
        {
            int targetX = event.GetPosition().x / square_length();
            int targetY = m_boardLength - 1 - event.GetPosition().y / square_length();

            m_to = targetY * m_boardLength + targetX;

            if(m_mode)
            {
                play_with_human();
            }
            else
            {
                play_with_ai();
            }

            m_icon[i]->finish_move(event.GetPosition(), true);
            break;
        }
    }
}

void Panel::on_move(wxMouseEvent& event)
{
    for(int i = 0; i < m_icon.size(); i++)
    {
        m_icon[i]->move(ScreenToClient(wxGetMousePosition()));
    }

    Refresh(true);
}

int Panel::square_length()
{
    int width = GetClientSize().GetWidth();
    int height = GetClientSize().GetHeight();
    int length = std::min(width, height) / m_boardLength;

    return length;
}

void Panel::draw_square(wxPaintDC& dc, int x, int y)
{
    static wxColor light = wxColor(255, 222, 173);
    static wxColor dark = wxColor(205, 133, 63);

    dc.SetPen(*wxTRANSPARENT_PEN);
    if (x % 2 == y % 2)
    {
        dc.SetBrush(wxBrush(light));
    } else
    {
        dc.SetBrush(wxBrush(dark));
    }

    dc.DrawRectangle(x * square_length(), y * square_length(),
        square_length(), square_length());
}

void Panel::show_problem(int status)
{
    std::string msg = "";
    msg += m_board.getActiveColor() ? "Black player " : "White player ";
    switch(status)
    {
        case Player::InCheck:
            msg += "under CHECK";
            break;

        case Player::Checkmate:
            msg += "under CHECKMATE\nGame OVER\n\n";
            m_board.changeActiveColor();
            msg += m_board.getActiveColor() ? "Black WINS" : "White WINS";
            break;

        case Player::Stalemate:
            msg += "under STALEMATE\nGame OVER\n\n";
            msg += "Tie";
            break;

        default:
            break;
    }

    wxNotificationMessage* show_detail = new wxNotificationMessage(wxT("Warning"), wxString(msg), this);
    show_detail->Show(2);

    if (status == Player::Checkmate or status == Player::Stalemate)
    {
        ((BoardFrame*)GetParent())->GetParent()->Show();
        m_parent->Destroy();
    }
}

void Panel::check_status()
{
    bool is_finished = false;
    int status = m_board.get_status();

    switch(status)
    {
        case Player::Checkmate:
            printf("Checkmate\n");
            is_finished = true;
            break;
        case Player::Stalemate:
            printf("Stalemate\n");
            is_finished = true;
            break;
        case Player::InCheck:
        case Player::Normal:
            break;
    }

    if(is_finished || status == Player::InCheck)
        show_problem(status);
}
