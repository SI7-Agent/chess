#ifndef PANEL_H
#define PANEL_H

#include <map>
#include <string>
#include <vector>
#include <wx/wx.h>
#include <wx/notifmsg.h>

#include "icon.h"
#include "../chessboard/chessboard.h"

class Panel : public wxPanel
{
public:
    Panel(wxFrame *parent, Chessboard& board, int f);

    Chessboard get_board() { return m_board; }
    void show_problem(int status);

    Chessboard m_board;

protected:
    void on_paint(wxPaintEvent& event);
    void on_mouse_down(wxMouseEvent& event);
    void on_mouse_up(wxMouseEvent& event);
    void on_move(wxMouseEvent& event);

    void play_with_human();
    void play_with_ai();

private:
    int square_length();
    void draw_square(wxPaintDC &dc, int x, int y);
    void load_figures();
    void check_status();

    int m_boardLength;
    int m_from, m_to, m_mode;
    std::vector<Icon*> m_icon;
    std::string m_origin;
    std::map<char, wxString> m_icon_path;
};

#endif // PANEL_H
