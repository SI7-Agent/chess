#include "transformdialog.h"

TransformDialog::TransformDialog(const wxString &title, Panel* p):
    wxDialog(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(300, 140))
{
    m_parent = p;

    int width = this->GetClientSize().GetX();
    int height = this->GetClientSize().GetY();
    int button_num = 4;

    int padding_y = 20;

    int b_width = height - 2 * padding_y;
    int b_height = height - 2 * padding_y;
    m_scale = b_width * 0.7;

    int padding_x = (width - button_num * b_width)/(button_num + 1);

    SetMaxSize(wxSize(GetSize().GetX(), GetSize().GetY()));
    SetMinSize(wxSize(GetSize().GetX(), GetSize().GetY()));

    wxButton *button_q = new wxButton(this, wx_queen_button, wxEmptyString, wxPoint(padding_x, padding_y), wxSize(b_width, b_height), wxBU_EXACTFIT);
    button_q->SetBitmapLabel(apply_icon(pawn_transform_queen, m_parent->get_board().getActiveColor()));
    Connect(wx_queen_button, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(TransformDialog::to_queen));

    wxButton *button_r = new wxButton(this, wx_rook_button, wxEmptyString, wxPoint(2 * padding_x + b_width, padding_y), wxSize(b_width, b_height), wxBU_EXACTFIT);
    button_r->SetBitmapLabel(apply_icon(pawn_transform_rook, m_parent->get_board().getActiveColor()));
    Connect(wx_rook_button, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(TransformDialog::to_rook));

    wxButton *button_k = new wxButton(this, wx_knight_button, wxEmptyString, wxPoint(3 * padding_x + 2 * b_width, padding_y), wxSize(b_width, b_height), wxBU_EXACTFIT);
    button_k->SetBitmapLabel(apply_icon(pawn_transform_knight, m_parent->get_board().getActiveColor()));
    Connect(wx_knight_button, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(TransformDialog::to_knight));

    wxButton *button_b = new wxButton(this, wx_bishop_button, wxEmptyString, wxPoint(4 * padding_x + 3 * b_width, padding_y), wxSize(b_width, b_height), wxBU_EXACTFIT);
    button_b->SetBitmapLabel(apply_icon(pawn_transform_bishop, m_parent->get_board().getActiveColor()));
    Connect(wx_bishop_button, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(TransformDialog::to_bishop));

    Centre();
}

const wxBitmap TransformDialog::apply_icon(int id, int color)
{
    wxImage img;
    wxString path;
    switch(id)
    {
        case pawn_transform_queen:
            color ? path = wxT("ui/img/black_queen.png") : path = wxT("ui/img/white_queen.png");
            img = wxImage(path, wxBITMAP_TYPE_ANY);
            break;

        case pawn_transform_rook:
            color ? path = wxT("ui/img/black_rook.png") : path = wxT("ui/img/white_rook.png");
            img = wxImage(path, wxBITMAP_TYPE_ANY);
            break;

        case pawn_transform_knight:
            color ? path = wxT("ui/img/black_knight.png") : path = wxT("ui/img/white_knight.png");
            img = wxImage(path, wxBITMAP_TYPE_ANY);
            break;

        case pawn_transform_bishop:
            color ? path = wxT("ui/img/black_bishop.png") : path = wxT("ui/img/white_bishop.png");
            img = wxImage(path, wxBITMAP_TYPE_ANY);
            break;
    }

    const wxBitmap pic = wxBitmap(img.Scale(m_scale, m_scale));
    return pic;
}

void TransformDialog::to_queen(wxCommandEvent & WXUNUSED(event))
{
    m_parent->m_board.set_pawn_transform(pawn_transform_queen);
    Destroy();
}

void TransformDialog::to_rook(wxCommandEvent & WXUNUSED(event))
{
    m_parent->m_board.set_pawn_transform(pawn_transform_rook);
    Destroy();
}

void TransformDialog::to_knight(wxCommandEvent & WXUNUSED(event))
{
    m_parent->m_board.set_pawn_transform(pawn_transform_knight);
    Destroy();
}

void TransformDialog::to_bishop(wxCommandEvent & WXUNUSED(event))
{
    m_parent->m_board.set_pawn_transform(pawn_transform_bishop);
    Destroy();
}

void TransformDialog::drop_window(wxCloseEvent & WXUNUSED(event))
{
    m_parent->m_board.set_pawn_transform(pawn_transform_queen);
    Destroy();
}
