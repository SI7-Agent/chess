#ifndef TransformDialog_H
#define TransformDialog_H

#include "../ui/panel.h"

class TransformDialog : public wxDialog
{
public:
    TransformDialog(const wxString& title, Panel* p);

    void to_queen(wxCommandEvent & WXUNUSED(event));
    void to_rook(wxCommandEvent & WXUNUSED(event));
    void to_knight(wxCommandEvent & WXUNUSED(event));
    void to_bishop(wxCommandEvent & WXUNUSED(event));

    void drop_window(wxCloseEvent & WXUNUSED(event));

private:
    const wxBitmap apply_icon(int id, int color);

    enum m_figures
    {
        pawn_transform_queen = 0,
        pawn_transform_rook,
        pawn_transform_knight,
        pawn_transform_bishop
    };

    enum m_buttons
    {
        wx_queen_button = 10,
        wx_rook_button,
        wx_knight_button,
        wx_bishop_button,
    };

    Panel* m_parent;
    int m_scale;
};

#endif // TRANSFORMDIALOG_H
